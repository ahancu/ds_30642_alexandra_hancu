import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SidebarComponent} from './sidebar/sidebar.component';
import {FooterComponent} from './footer/footer.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ReportsComponent} from './reports/reports.component';
import {RouterModule, Routes} from '@angular/router';
import {PageComponent} from './page.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import { PlantsComponent } from './plants/plants.component';
import { GardenersComponent } from './gardeners/gardeners.component';
import { MapComponent } from './map/map.component';




@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    RouterModule,
    NgbModule.forRoot()
  ],
  declarations: [PageComponent, DashboardComponent, FooterComponent,
     ReportsComponent, SidebarComponent, PlantsComponent, GardenersComponent, MapComponent],
  exports: [PageComponent]
})
export class PageModule {
}
