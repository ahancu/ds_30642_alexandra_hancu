import { Component, OnInit } from '@angular/core';
import {PrinterService} from '../../printer.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

  constructor(private printerService: PrinterService) { }

  ngOnInit() {
  }

  generatePlantsPdf() {
    this.printerService.plantsPdf().subscribe(data => console.log('plants pdf done? ' + data));
  }

  generatePlotsPdf() {
    this.printerService.plotsPdf().subscribe(data => console.log('plots pdf done? ' + data));
  }

  generateGardenersPdf() {
    this.printerService.gardenerPdf().subscribe(data => console.log('gardener pdf done? ' + data));
  }
}
