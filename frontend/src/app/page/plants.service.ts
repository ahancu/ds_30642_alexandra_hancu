import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {ToPlant} from '../gardener/gardener.component';

@Injectable({
  providedIn: 'root'
})
export class PlantsService {

  constructor(private http: HttpClient) {
  }

  getPlants(): Observable<any> {
    return this.http.get<Observable<any>>('http://localhost:8080/plants');
  }

  newPlant(plant: Plant): Observable<any> {
    return this.http.post('http://localhost:8080/plants', {
      ['name']: plant.name,
      ['type']: plant.type,
      ['dimension']: plant.size,
      ['wateringAmount']: plant.water,
      ['wateringInterval']: plant.waterInterval,
      ['noSeeds']: 100
    });
  }

  delete(plantId: any): Observable<any> {
    return this.http.delete('http://localhost:8080/plants/' + plantId);
  }

  plant(toPlant: ToPlant): Observable<any> {
    return this.http.put('http://localhost:8080/plants', {
      ['plantId']: toPlant.plantId,
      ['plotId']: toPlant.plotId
    });
  }
}

export class Plant {
  id: number;
  name: string;
  type: string;
  size: number;
  water: number;
  waterInterval: number;
  noSeeds: number;
}
