import { TestBed } from '@angular/core/testing';

import { GardenersService } from './gardeners.service';

describe('GardenersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GardenersService = TestBed.get(GardenersService);
    expect(service).toBeTruthy();
  });
});
