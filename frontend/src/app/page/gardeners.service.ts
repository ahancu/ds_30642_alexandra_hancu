import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Gardener} from './gardeners/gardeners.component';

@Injectable({
  providedIn: 'root'
})
export class GardenersService {

  constructor(private http: HttpClient) { }

  getGardeners(): Observable<any> {
    return this.http.get<Observable<any>>('http://localhost:8080/gardeners');
  }

  delete(id: number): Observable<any> {
    return this.http.delete('http://localhost:8080/gardeners/' + id);
  }

  newGardener(gardener: Gardener): Observable<any> {
    return this.http.post('http://localhost:8080/gardeners', {
      ['name']: gardener.name,
      ['username']: gardener.username,
      ['password']: gardener.password,
      ['teamId']: gardener.teamId,
    });
  }

  getTeams(): Observable<any> {
    return this.http.get<Observable<any>>('http://localhost:8080/teams');
  }
}
