import { Component, OnInit } from '@angular/core';
import {a, T} from '@angular/core/src/render3';
import {PlantsService} from '../plants.service';
import {NgForm} from '@angular/forms';

class Plant {
  id: number;
  name: string;
  type: string;
  size: number;
  water: number;
  waterInterval: number;
  noSeeds: number;
}

@Component({
  selector: 'app-plants',
  templateUrl: './plants.component.html',
  styleUrls: ['./plants.component.scss']
})
export class PlantsComponent implements OnInit {
  allPlants: Plant[];
  newPlant: Plant;

  constructor(private plantsService: PlantsService) { }

  ngOnInit() {
    this.refreshPlants();
    this.newPlant = new Plant();
  }

  refreshPlants() {
    this.plantsService.getPlants().subscribe(data => {
      console.log(data);
      this.allPlants = data;
    });
  }

  deletePlant(plant: Plant) {
      console.log('clicked delete ' + plant.name + ', id:' + plant.id);
      this.plantsService.delete(plant.id).subscribe(data => console.log('deleted ' + plant.name + data));
      this.refreshPlants();
  }

  addNewPlant(formData: NgForm) {
    this.newPlant.name = formData.controls.name.value;
    this.newPlant.type = formData.controls.type.value;
    this.newPlant.size = formData.controls.size.value;
    this.newPlant.water = formData.controls.water.value;
    this.newPlant.waterInterval = formData.controls.waterInterval.value;
    console.log('new p' + this.newPlant.name + this.newPlant.type + this.newPlant.size + this.newPlant.water + this.newPlant.waterInterval);
    this.plantsService.newPlant(this.newPlant).subscribe((data) => console.log('response from new plant request: ' + data));
    this.refreshPlants();
  }
}
