import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {GardenersService} from '../gardeners.service';

@Component({
  selector: 'app-gardeners',
  templateUrl: './gardeners.component.html',
  styleUrls: ['./gardeners.component.scss']
})

export class GardenersComponent implements OnInit {
  allGardeners: Gardener[];
  newGardener: Gardener;
  teams: Team[];

  constructor(private gardenersService: GardenersService) { }

  ngOnInit() {
    this.newGardener = new Gardener();
    this.refreshGardeners();
    this.refreshTeams();
  }

  refreshGardeners() {
    this.gardenersService.getGardeners().subscribe(data => {
      console.log(data);
      this.allGardeners = data;
    });
  }

  refreshTeams() {
    this.gardenersService.getTeams().subscribe(data => {
      console.log(data);
      this.teams = data;
    });
  }

  addNewGardener(formData: NgForm) {
    console.log('clicked add new gardener');
    this.newGardener.name = formData.controls.name.value;
    this.newGardener.username = formData.controls.username.value;
    this.newGardener.password = formData.controls.password.value;
    if (formData.controls.team.value != null) {
      this.newGardener.teamId = formData.controls.team.value;
    }
    console.log('newg: ' + this.newGardener.name + this.newGardener.username + this.newGardener.password + this.newGardener.teamId);
    this.gardenersService.newGardener(this.newGardener).subscribe((data) =>
      console.log('added ' + this.newGardener.name + ' response: ' + data)
    );
    this.refreshGardeners();
  }

  deleteGardener(gardener: Gardener) {
    console.log('clicked delete for gardener ' + gardener.id + ' ' + gardener.name);
    this.gardenersService.delete(gardener.id).subscribe((data) =>
      console.log('deleted ' + gardener.id + ' response: ' + data));
    this.refreshGardeners();
  }
}

export class Gardener {
  id: number;
  name: string;
  username: string;
  password: string;
  teamId: number;
}

export class Team {
  id: number;
  gardeners: Gardener[];
}
