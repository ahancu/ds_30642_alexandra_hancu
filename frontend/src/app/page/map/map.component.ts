import { Component, OnInit } from '@angular/core';
import {MapService} from '../../map.service';
import {Gardener} from '../gardeners/gardeners.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  matrix: any[][];
  constructor(private mapService: MapService) { }

  ngOnInit() {
    this.getMapData();
  }

  getMapData() {
    this.mapService.getMap().subscribe(data => {
      this.matrix = data;
      console.log(data);
    });
  }

}

export class MapObj {
  plotId: number;
  gardener: string;
  plant: string;
}
