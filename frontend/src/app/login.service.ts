import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private username;
  private isAdministrator = false;
  private url = 'http://localhost:8081/user/login';

  constructor(private http: HttpClient) {
  }

  setUsername(value: String) {
    this.username = value;
  }

  login(username: string, password: string) {
    return this.http.post('//localhost:8080/login', {username, password});
  }

  isAdmin(username: string) {
    this.isAdministrator = username.localeCompare("admin") === 0;
    return this.isAdministrator;
  }
}
