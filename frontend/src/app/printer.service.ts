import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PrinterService {

  constructor(private http: HttpClient) { }

  gardenerPdf(): Observable<any> {
    return this.http.get('http://localhost:8080/report/gardener');
  }

  plotsPdf(): Observable<any> {
    return this.http.get('http://localhost:8080/report/plots');
  }

  plantsPdf(): Observable<any> {
    return this.http.get('http://localhost:8080/report/plants');
  }
}
