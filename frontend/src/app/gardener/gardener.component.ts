import { Component, OnInit } from '@angular/core';
import {Plant, PlantsService} from '../page/plants.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-gardener',
  templateUrl: './gardener.component.html',
  styleUrls: ['./gardener.component.scss']
})
export class GardenerComponent implements OnInit {
  plants: Array<string> = [];
  allPlants: Plant[];
  toPlant: ToPlant;
  plantName: String;

  constructor(private plantsService: PlantsService) { }

  ngOnInit() {
    this.refreshPlants();
  }

  refreshPlants() {
    this.plantsService.getPlants().subscribe(data => {
      console.log(data);
      this.allPlants = data;
      let plant: Plant;
      for (plant of this.allPlants) {
        this.plants.push(plant.name);
      }
    });
  }

  plantSeed(formData: NgForm) {
    /*let plant: Plant;
    for (plant of this.allPlants) {
      if ( plant.name.localeCompare(formData.controls.plantList.value) === 0) {
        this.toPlant.plantId = plant.id;
        /!*this.toPlant.plotId = formData.controls.plot.value;*!/
        console.log('plant id: ' + this.toPlant.plantId + ' plotId ' + this.toPlant.plotId);
      }
    }*/
    this.toPlant.plotId = formData.controls.plot.value;
    console.log('loooost it');
    /*
    this.plantsService.plant(this.toPlant).subscribe(data => {
      console.log(data);
    });
    */
  }

  setPlantId(id: number) {
    this.toPlant.plantId = id;
    console.log('plant id set to ' + id);
  }
}

export class ToPlant {
  plantId: number;
  plotId: number;
}

export class OutputPlot {
  id: number;
  fromX: number;
  toX: number;
  fromY: number;
  toY: number;
  gardenId: number;
  plantId: number;
}
