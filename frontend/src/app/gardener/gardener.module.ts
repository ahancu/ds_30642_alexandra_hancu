import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GardenerComponent } from './gardener.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {LoginComponent} from '../login/login.component';

const gardenerRoutes: Routes = [
  {path: 'gardener', component: GardenerComponent}
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(gardenerRoutes),
    ReactiveFormsModule,
    HttpClientModule
  ],
  declarations: [GardenerComponent]
})
export class GardenerModule { }
