import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {PageComponent} from './page/page.component';
import {LoginComponent} from './login/login.component';
import {DashboardComponent} from './page/dashboard/dashboard.component';
import {ReportsComponent} from './page/reports/reports.component';
import {PlantsComponent} from './page/plants/plants.component';
import {GardenerComponent} from './gardener/gardener.component';
import {GardenersComponent} from './page/gardeners/gardeners.component';
import {MapComponent} from './page/map/map.component';
import {LogoutComponent} from './logout/logout.component';

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'reports', component: ReportsComponent},
  {path: 'plants', component: PlantsComponent},
  {path: 'gardener', component: GardenerComponent},
  {path: 'gardeners', component: GardenersComponent},
  {path: 'map', component: MapComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),

  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
