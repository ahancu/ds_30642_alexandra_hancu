import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login.component';
import {DashboardComponent} from '../page/dashboard/dashboard.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

const loginRoutes: Routes = [
  {path: 'login', component: LoginComponent}
]


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(loginRoutes),
    ReactiveFormsModule,
    HttpClientModule
  ],
  declarations: [LoginComponent]

})
export class LoginModule { }
