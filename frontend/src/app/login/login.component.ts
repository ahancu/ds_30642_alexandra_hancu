import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from '../login.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  user: User;

  constructor(private router: Router, private loginService: LoginService) {
    this.username = '';
    this.password = '';
  }

  ngOnInit() {
    console.log(this.password + this.username);
  }

  public login(username, password) {
    console.log('User' + this.password + this.username);
    this.loginService.login(username, password)
      .subscribe((data) => {
        if (data != null && data['success'] === true) {
          console.log(sessionStorage.getItem('username'));
          localStorage.setItem('username', username);
          this.loginService.setUsername(username);
          if (this.loginService.isAdmin(username)) {
            this.router.navigate(['dashboard']);
          } else {
            this.router.navigate(['gardener']);
          }
        } else {
          console.error(`Login failed.`);
        }
      });
  }
}

  class User {
  id: number;
  username: string;
  password: string;
  role: string;
}
