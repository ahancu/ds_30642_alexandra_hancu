import server.services.GardenService;
import model.Garden;
import server.repos.GardenRepo;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestGardenService {
    private GardenService gardenService;
    private GardenRepo gardenRepo = mock(GardenRepo.class);
    private int testID = 11;
    @Before
    public void setUp(){
        Garden toReturn = new Garden();
        toReturn.setId(testID);
        toReturn.setLength(8);
        toReturn.setWidth(5);
        when(gardenRepo.getOne(testID)).thenReturn(toReturn);
        gardenService = new GardenService(gardenRepo);
    }
    @Test
    public void testGetGarden(){
        try{
            Garden myReturnedObject = gardenService.getGarden(testID);
            assertNotNull(myReturnedObject);//check if the object is != null
            //checks if the returned object is of class Expression
            assertTrue( myReturnedObject.getLength() == 8);
            assertTrue(myReturnedObject.getWidth() == 5);
        }catch(Exception e){
            // let the test fail, if your function throws an Exception.
            fail("got Exception");
        }
    }

}
