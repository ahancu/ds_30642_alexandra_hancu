import model.Employee;
import server.services.LoginService;
import org.junit.Before;
import org.junit.Test;
import server.repos.EmployeeRepo;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestLoginService {
    private LoginService loginService = null;
    private EmployeeRepo employeeRepo = mock(EmployeeRepo.class);

    @Before
    public void setUp(){
        when(employeeRepo.getPassword("sSimona")).thenReturn("123");
        when(employeeRepo.getPassword("gAna")).thenReturn("girl1");
        Employee expected = new Employee("gAna", "girl1", "Ana");
        expected.setId(4);
        when(employeeRepo.getByUsername("gAna")).thenReturn(expected);
        loginService = new LoginService(employeeRepo);
    }

    @Test
    public void testLogin(){
        assertEquals(true, loginService.login("sSimona","123"));
        assertEquals(true, loginService.login("gAna","girl1"));
    }

    @Test
    public void testGetEmployee(){
        Employee expected = new Employee("gAna", "girl1", "Ana");
        expected.setId(4);
        assertEquals(expected, loginService.getEmployee("gAna"));
    }

}
