import client.HtmlPrinter;
import client.IPrinter;
import client.PrinterFactory;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TestPrinterFactory {
    PrinterFactory printerFactory = null;

    @Before
    public void setUp(){
        printerFactory = new PrinterFactory();
    }


    @Test
    public void testGetPrinter(){
        try{
            IPrinter myReturnedObject = printerFactory.getPrinter(true);
            assertNotNull(myReturnedObject);//check if the object is != null
            //checks if the returned object is of class Expression
            assertTrue( myReturnedObject.getClass() == HtmlPrinter.class);
        }catch(Exception e){
            // let the test fail, if your function throws an Exception.
            fail("got Exception, i want an HtmlPrinter");
        }
    }
}
