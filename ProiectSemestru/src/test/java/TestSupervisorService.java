import model.Employee;
import model.Supervisor;
import model.Team;
import org.junit.Before;
import org.junit.Test;
import server.services.SupervisorService;
import server.repos.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestSupervisorService {
    private SupervisorService supervisorService;
    private PlotRepo plotRepo = mock(PlotRepo.class);
    private PlantRepo plantRepo = mock(PlantRepo.class);
    private TaskRepo taskRepo = mock(TaskRepo.class);
    private TeamRepo teamRepo = mock(TeamRepo.class);
    private SupervisorRepo supervisorRepo = mock(SupervisorRepo.class);
    private GardenerRepo gardenerRepo = mock(GardenerRepo.class);
    private EmployeeRepo employeeRepo = mock(EmployeeRepo.class);

    @Before
    public void setUp(){
        supervisorService = new SupervisorService(supervisorRepo, plantRepo, gardenerRepo, employeeRepo, teamRepo, plotRepo, taskRepo);

        Team team = new Team();
        team.setId(7);

        Employee employee = new Employee();
        employee.setId(6);
        employee.setName("Simona");
        employee.setUsername("sSimona");
        employee.setPassword("123");

        Supervisor toReturn2 = new Supervisor();
        toReturn2.setEmployee(employee);
        toReturn2.setTeam(team);
        toReturn2.setId(10);

        when(supervisorRepo.getByEmployee(employee)).thenReturn(toReturn2);

    }

    @Test
    public void testGetSupervisor(){
        Team team = new Team();
        team.setId(7);

        Employee employee = new Employee();
        employee.setId(6);
        employee.setName("Simona");
        employee.setUsername("sSimona");
        employee.setPassword("123");

        Supervisor toReturn2 = new Supervisor();
        toReturn2.setEmployee(employee);
        toReturn2.setTeam(team);
        toReturn2.setId(10);

        assertEquals(toReturn2, supervisorService.getSupervisor(employee));
    }
}
