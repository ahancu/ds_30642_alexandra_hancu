import model.Garden;
import model.Plot;
import model.Team;
import org.junit.Before;
import org.junit.Test;
import server.repos.GardenRepo;
import server.services.MapService;
import server.repos.GardenerRepo;
import server.repos.PlotRepo;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestMapService {
    private MapService mapService;
    private PlotRepo plotRepo = mock(PlotRepo.class);
    private GardenerRepo gardenerRepo = mock(GardenerRepo.class);
    private GardenRepo gardenRepo = mock(GardenRepo.class);
    private List<Plot> expected;
    private Garden garden;
    @Before
    public void setUp(){
        mapService = new MapService(plotRepo, gardenerRepo, gardenRepo);

        Team t = new Team();
        t.setId(7);
        garden = new Garden(5,8, t);
        garden.setId(11);
        Plot p1 = new Plot(garden, 0,2,0,2);
        Plot p2 = new Plot(garden, 3,4,0,2);
        Plot p3 = new Plot(garden, 0, 2, 3,5);
        Plot p4 = new Plot(garden, 0, 2, 6, 7);
        Plot p5 = new Plot(garden, 3, 4, 3,7);
        expected = new ArrayList<>();
        expected.add(p1);
        expected.add(p2);
        expected.add(p3);
        expected.add(p4);
        expected.add(p5);

        when(plotRepo.getAll()).thenReturn(expected);
    }

    @Test
    public void testGetPlots(){
        assertEquals(expected, mapService.getPlots(garden));
    }
}
