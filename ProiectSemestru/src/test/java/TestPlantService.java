import model.Garden;
import model.Plant;
import model.Plot;
import model.Team;
import org.junit.Before;
import org.junit.Test;
import server.services.PlantService;
import server.repos.PlantRepo;
import server.repos.PlotRepo;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestPlantService {
    PlantService plantService;
    PlotRepo plotRepo = mock(PlotRepo.class);
    PlantRepo plantRepo = mock(PlantRepo.class);

    @Before
    public void setUp(){
        plantService = new PlantService(plantRepo, plotRepo);
        Plant plant1 = new Plant("cereal","rice","3x10",4,3,10);
        Plant plant2 = new Plant("flower", "tulip", "30cm",10,3, 10);
        Plant plant3 = new Plant("flower", "rose", "50 cm", 5, 8, 10);
        List<Plant> plants = new ArrayList<>();
        plant1.setId(1);
        plant2.setId(2);
        plant3.setId(3);
        plants.add(plant1);
        plants.add(plant2);
        plants.add(plant3);
        when(plantRepo.getAll()).thenReturn(plants);

        when(plantRepo.getOne(2)).thenReturn(plant2);

        Team t = new Team(); t.setId(7);
        Garden garden = new Garden(5,8, t); garden.setId(11);
        Plot plot = new Plot(garden, 0, 2, 3,5); plot.setId(14); plot.setPlant(plant2);
        when(plotRepo.update(14, plot)).thenReturn(plot);

        plant2.decreaseNoSeeds();
        when(plantRepo.update(plant2.getId(), plant2)).thenReturn(plant2);
    }

    @Test
    public void testGetPlants(){
        Plant plant1 = new Plant("cereal","rice","3x10",4,3,10);
        Plant plant2 = new Plant("flower", "tulip", "30cm",10,3, 10);
        Plant plant3 = new Plant("flower", "rose", "50 cm", 5, 8, 10);
        List<Plant> expected = new ArrayList<>();
        plant1.setId(1);
        plant2.setId(2);
        plant3.setId(3);
        expected.add(plant1);
        expected.add(plant2);
        expected.add(plant3);
        assertEquals(expected, plantRepo.getAll());
    }

    @Test
    public void testAddPlant(){
        Team t = new Team(); t.setId(7);
        Garden garden = new Garden(5,8, t); garden.setId(11);
        Plot plotInit = new Plot(garden, 0, 2, 3,5); plotInit.setId(14);
        Plant plant2 = new Plant("flower", "tulip", "30cm",10,3, 10); plant2.setId(2);
        Plot expected = plotInit;
        expected.setPlant(plant2);
        assertEquals(expected, plantService.addPlant(plotInit, plant2));
    }

    @Test
    public void testUpdatePlant(){
        Plant input = new Plant("flower", "tulip", "30cm",10,3, 10); input.setId(2); input.decreaseNoSeeds();
        Plant expected = input;
        assertEquals(expected, plantService.updatePlant(input));
    }

}
