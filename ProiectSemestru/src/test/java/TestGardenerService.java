import model.Employee;
import model.Garden;
import model.Gardener;
import model.Team;
import org.junit.Before;
import org.junit.Test;
import server.repos.PlotRepo;
import server.services.GardenerService;
import server.repos.GardenRepo;
import server.repos.GardenerRepo;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestGardenerService {
    private GardenerService gardenerService;
    private GardenerRepo gardenerRepo = mock(GardenerRepo.class);
    private GardenRepo gardenRepo = mock(GardenRepo.class);
    private PlotRepo plotRepo = mock(PlotRepo.class);

    private int testID = 11;
    @Before
    public void setUp(){
        gardenerService = new GardenerService(gardenRepo, gardenerRepo, plotRepo);

        Garden toReturn = new Garden();
        toReturn.setId(testID);
        toReturn.setLength(8);
        toReturn.setWidth(5);
        Team team = new Team();
        team.setId(7);
        when(gardenRepo.getGarden4Team(team)).thenReturn(toReturn);

        Employee employee = new Employee();
        employee.setId(4);
        employee.setName("Ana");
        employee.setUsername("gAna");
        employee.setPassword("girl1");
        Gardener toReturn2 = new Gardener();
        toReturn2.setEmployee(employee);
        toReturn2.setMoney(0);
        toReturn2.setTeam(team);
        toReturn2.setTask(null);
        when(gardenerRepo.getByEmployee(employee)).thenReturn(toReturn2);
    }
    @Test
    public void testGetGarden(){
        try{
            Gardener gardener = new Gardener();
            Team team = new Team();
            team.setId(7);
            gardener.setTeam(team);
            Garden expected = new Garden();
            expected.setId(testID);
            expected.setLength(8);
            expected.setWidth(5);
            assertEquals(expected, gardenerService.getGarden(gardener));
        }catch(Exception e){
            // let the test fail, if your function throws an Exception.
            fail("got Exception");
        }
    }

    @Test
    public void testGetGardener(){
        try{
            Team team = new Team();
            team.setId(7);

            Employee employee = new Employee();
            employee.setId(4);
            employee.setName("Ana");
            employee.setUsername("gAna");
            employee.setPassword("girl1");

            Gardener toReturn2 = new Gardener();
            toReturn2.setEmployee(employee);
            toReturn2.setMoney(0);
            toReturn2.setTeam(team);
            toReturn2.setTask(null);

            assertEquals(toReturn2, gardenerService.getGardener(employee));

        }catch(Exception e){
            fail("got Exception");
        }
    }
}
