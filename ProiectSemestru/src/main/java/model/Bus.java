package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Bus implements Serializable {//object that transports data trough socket
    private MessageType messageType;
    private String text;
    private List<Object> data;

    public Bus(){
        data = new ArrayList<>();
    }

    public Bus(MessageType messageType, String text, List<Object> data) {
        this.messageType = messageType;
        this.text = text;
        this.data = data;
    }

    public void insertItem(Object obj){
        data.add(obj);
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

    @Override
    public String toString(){
        return "[BUS] " + messageType + "\n\t" + data;
    }
}
