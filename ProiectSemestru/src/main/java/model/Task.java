package model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
public class Task implements Serializable {
    @Id
    @GeneratedValue
    private int id;
    @ManyToOne(targetEntity = Plot.class)
    @JoinColumn(name="plot_id")
    private Plot plot;
    private int timeToComplete; //mins
    private float reward; //money4theTask
    private boolean completed;

    public Task(Plot plot, int timeToComplete, float reward, Gardener completedBy) {
        this.plot = plot;
        this.timeToComplete = timeToComplete;
        this.reward = reward;
        completed = false;
    }

    public Task() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Plot getPlot() {
        return plot;
    }

    public void setPlot(Plot plot) {
        this.plot = plot;
    }

    public int getTimeToComplete() {
        return timeToComplete;
    }

    public void setTimeToComplete(int timeToComplete) {
        this.timeToComplete = timeToComplete;
    }

    public float getReward() {
        return reward;
    }

    public void setReward(float reward) {
        this.reward = reward;
    }

    @Override
    public String toString() {
        return "<<Task>> reward:"+ reward +"time to complete" + timeToComplete;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
