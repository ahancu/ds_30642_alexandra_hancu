package model;

import java.io.Serializable;

public class MapObj implements Serializable {
    private int plotId;
    private Gardener gardener;
    private Plant plant;

    public MapObj(int plotId, Gardener gardener, Plant plant) {
        this.plotId = plotId;
        this.gardener = gardener;
        this.plant = plant;
    }

    public MapObj() {
    }

    public int getPlotId() {
        return plotId;
    }

    public void setPlotId(int plotId) {
        this.plotId = plotId;
    }

    public Gardener getGardener() {
        return gardener;
    }

    public void setGardener(Gardener gardener) {
        this.gardener = gardener;
    }

    public Plant getPlant() {
        return plant;
    }

    public void setPlant(Plant plant) {
        this.plant = plant;
    }

    @Override
    public String toString() {
        if(gardener==null){
            if(plant==null){
                return ""+plotId;
            }
            else {
                return plotId+"\n"+plant.getName();
            }
        }
        else {
            if(plant==null){

                return plotId+"\n"+gardener.getEmployee().getName();
            }
            else {
                return plotId+"\n"+gardener.getEmployee().getName()+"\n"+plant.getName();
            }
        }
    }
}
