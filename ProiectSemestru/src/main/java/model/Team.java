package model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Team implements Serializable {
    @Id
    @GeneratedValue
    private int id;

    public Team() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        Team team = null;
        try{
            team = (Team) obj;
        }catch (Exception e){
            return false;
        }
        return team.getId()==id;
    }
}
