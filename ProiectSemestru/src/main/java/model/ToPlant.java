package model;

public class ToPlant {
    public int plantId;
    public int plotId;

    public ToPlant(int plantId, int plotId) {
        this.plantId = plantId;
        this.plotId = plotId;
    }

    public ToPlant() {
    }
}
