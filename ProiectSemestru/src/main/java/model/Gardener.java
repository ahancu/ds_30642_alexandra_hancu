package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table
public class Gardener implements Serializable{
    @Id
    @GeneratedValue
    private int id;
    private float money;
    @OneToOne(targetEntity = Employee.class)
    @JoinColumn(name = "employee_id")
    private Employee employee;
    @ManyToOne(targetEntity = Team.class)
    @JoinColumn(name ="team_id")
    private Team team;
    @OneToOne(targetEntity = Task.class)
    @JoinColumn(name = "task_id")
    private Task task;

    public Gardener(float money, Employee employee, Team team) {
        this.money = money;
        this.employee = employee;
        this.team = team;
    }

    public Gardener() {
    }

    public int getId() {
        return id;
    }

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    @Override
    public String toString() {
        return "<<Gardener>> " + employee.getName() + ", id: " + id +"\n";
    }

    @Override
    public boolean equals(Object obj) {
        Gardener gardener = null;
        try{
            gardener = (Gardener) obj;
        }catch (Exception e){
            return false;
        }
        return gardener.team.equals(team) && gardener.getMoney()==money && gardener.getId() == id;
    }
}
