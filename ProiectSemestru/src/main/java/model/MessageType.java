package model;

public enum MessageType {
    notifcation, login, getEmployee,
    addPlant, getPlant, getTeamMembers,
    getGardenerInfo, getPlantList,
    getGarden, getPlots,
    makeGardenMatrix, newPlant,
    newGardener3, newGardener2, getTeam,
    getGardenByTeam, newTask, getGardenerByEmployee,
    getSupervisor, updatePlant, updateSupervisor,
    getRefillRequestByGardener, newRequest, updateRequest;
}
