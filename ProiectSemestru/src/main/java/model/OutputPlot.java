package model;

public class OutputPlot {
    public int id;
    public int fromX;
    public int toX;
    public int fromY;
    public int toY;
    public int gardenId;
    public int plantId;

    public OutputPlot(int id, int fromX, int toX, int fromY, int toY, int gardenId, int plantId) {
        this.id = id;
        this.fromX = fromX;
        this.toX = toX;
        this.fromY = fromY;
        this.toY = toY;
        this.gardenId = gardenId;
        this.plantId = plantId;
    }

    public OutputPlot() {
    }
}
