package model;

import server.services.LoginService;

public abstract class Observer {
    protected LoginService subject;
    public abstract void update();
}
