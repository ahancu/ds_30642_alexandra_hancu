package model;

import java.util.List;

public class OutputTeam {
    public int id;
    public List<OutputGardener> gardeners;

    public OutputTeam(int id, List<OutputGardener> gardeners) {
        this.id = id;
        this.gardeners = gardeners;
    }

    public OutputTeam() {
    }
}
