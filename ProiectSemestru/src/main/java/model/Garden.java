package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Garden implements Serializable{

    @Id
    @GeneratedValue
    private int id;
    private int width;
    private int length;
    @OneToOne
    private Team team;

    public Garden(int width, int length, Team team) {
        this.width = width;
        this.length = length;
        this.team = team;
    }

    public Garden() {
    }

    public Team getTeam() {
        return team;
    }

    public int getId() {
        return id;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "<<Garden>> width:" + width + ", length: " + length + "\n";
    }

    @Override
    public boolean equals(Object obj) {
        Garden param = null;
        try{
            param = (Garden) obj;
        }catch (Exception e){
            return false;
        }
        return param.id == id && param.length == length && param.width == width;
    }
}
