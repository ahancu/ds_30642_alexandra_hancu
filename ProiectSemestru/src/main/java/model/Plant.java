package model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Plant implements Serializable {
    @Id
    @GeneratedValue
    private int id;
    private String type;
    protected String name;
    private String dimension;
    private int wateringInterval;
    private int wateringAmount;
    private int noSeeds;
    @ManyToOne
    @JoinColumn(name="supervisor_id")
    private Supervisor supervisor;
    @ManyToOne
    @JoinColumn(name="refillRequest_id")
    private RefillRequest refillRequest;

    public Plant(){

    }

    public Plant(String type, String name, String dimension, int wateringInterval, int wateringAmount, int noSeeds) {
        this.type = type;
        this.name = name;
        this.dimension = dimension;
        this.wateringInterval = wateringInterval;
        this.wateringAmount = wateringAmount;
        this.noSeeds = noSeeds;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public int getWateringInterval() {
        return wateringInterval;
    }

    public void setWateringInterval(int wateringInterval) {
        this.wateringInterval = wateringInterval;
    }

    public int getWateringAmount() {
        return wateringAmount;
    }

    public void setWateringAmount(int wateringAmount) {
        this.wateringAmount = wateringAmount;
    }

    public int getNoSeeds() {
        return noSeeds;
    }

    public void setNoSeeds(int noSeeds) {
        this.noSeeds = noSeeds;
    }

    public void decreaseNoSeeds(){
        noSeeds--;
    }

    @Override
    public String toString() {
        return "<<PLANT>> "+name+id+" type: "+type+", size:"+dimension+", water me "+wateringAmount+" every "+wateringInterval+" mins.\n";
    }


    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        Plant plant = null;
        try{
            plant = (Plant) obj;
        }catch (Exception e){
            return false;
        }
        if(plant == null)
            return false;
        return plant.id == id && plant.wateringInterval == wateringInterval && plant.wateringAmount == wateringAmount &&
                plant.type.compareTo(type) == 0 && plant.name.compareTo(name) == 0 && plant.dimension.compareTo(dimension) == 0
                &&plant.noSeeds == noSeeds;
    }

    public Supervisor getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Supervisor supervisor) {
        this.supervisor = supervisor;
    }
}
