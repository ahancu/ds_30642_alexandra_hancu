package model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
public class Employee implements Serializable {
    @Id
    @GeneratedValue
    private int id;
    private String username;
    private String password;
    private String name;

    public Employee(String username, String password, String name) {
        this.username = username;
        this.password = password;
        this.name = name;
    }

    public Employee() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public boolean equals(Object obj) {
        Employee employee = null;
        try{
            employee = (Employee) obj;
        }catch (Exception e){
            return false;
        }
        return employee.id == id && employee.name.compareTo(name) == 0 && employee.username.compareTo(username) == 0
                && employee.password.compareTo(password) == 0;
    }
}
