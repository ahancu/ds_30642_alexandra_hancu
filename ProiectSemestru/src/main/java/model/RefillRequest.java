package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table
public class RefillRequest implements Serializable{
    @Id
    private int id;

    @OneToOne(targetEntity = Gardener.class)
    @JoinColumn(name = "gardener_id")
    private Gardener gardener;
    @OneToMany(mappedBy = "refillRequest")
    private List<Plant> plants;
    @ManyToOne
    @JoinColumn(name="supervisor_id")
    private Supervisor supervisor;

    private int ranking;

    public RefillRequest(Gardener gardener, List<Plant> plants) {
        this.gardener = gardener;
        this.plants = plants;
        ranking = plants.size();
    }

    public RefillRequest(){

    }

    @Override
    public String toString() {
        return gardener.getEmployee().getName() + "requested refill for: \n" + plants;
    }

    public int updateRank(){
        ranking = plants.size();
        return ranking;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Gardener getGardener() {
        return gardener;
    }

    public void setGardener(Gardener gardener) {
        this.gardener = gardener;
    }

    public List<Plant> getPlants() {
        return plants;
    }

    public void setPlants(List<Plant> plants) {
        this.plants = plants;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public void addPlant(Plant plant){
        plants.add(plant);
    }

    public Supervisor getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Supervisor supervisor) {
        this.supervisor = supervisor;
    }
}
