package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@Table
public class Supervisor extends Observer implements  Serializable{
    @Id
    @GeneratedValue
    private int id;
    @OneToOne
    @JoinColumn(name="employee_id")
    private Employee employee;
    @OneToOne
    @JoinColumn(name="team_id")
    private Team team;
    @OneToMany(fetch = FetchType.EAGER, targetEntity = RefillRequest.class, mappedBy = "supervisor")
    private List<RefillRequest> refillList;

    public Supervisor(Employee employee, Team team) {
        this.employee = employee;
        this.team = team;
        refillList = new ArrayList<>();
    }

    public Supervisor() {

    }

    public int getId() {
        return id;
    }


    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @Override
    public void update() {

    }

    @Override
    public String toString() {
        return "<<Supervisor>> " + employee +" team: " + team;
    }

    public void addToRefillList(RefillRequest request) {
        refillList.add(request);
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<RefillRequest> getRefillList() {
        return refillList;
    }

    public void setRefillList(List<RefillRequest> refillList) {
        this.refillList = refillList;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        Supervisor supervisor = null;
        try{
            supervisor = (Supervisor) obj;
        }catch (Exception e){
            return false;
        }
        return supervisor.id == id && supervisor.team.equals(team) &&
                ((supervisor.refillList == null && refillList==null)||supervisor.refillList.equals(refillList) || (supervisor.refillList.isEmpty() && refillList.isEmpty())) &&
                supervisor.employee.equals(employee);
    }
}
