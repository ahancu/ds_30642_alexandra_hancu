package model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Plot implements Serializable {
    @Id
    @GeneratedValue
    private int id;
    private int fromX;
    private int toX;
    private int fromY;
    private int toY;
    @ManyToOne(targetEntity = Garden.class)
    private Garden garden;
    @ManyToOne(targetEntity = Plant.class)
    private Plant plant;

    public Plot(Garden garden, int fromX, int toX, int fromY, int toY) {
        this.garden = garden;
        this.fromX = fromX;
        this.toX = toX;
        this.fromY = fromY;
        this.toY = toY;
    }

    public Plot() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFromX() {
        return fromX;
    }

    public void setFromX(int fromX) {
        this.fromX = fromX;
    }

    public int getToX() {
        return toX;
    }

    public void setToX(int toX) {
        this.toX = toX;
    }

    public int getFromY() {
        return fromY;
    }

    public void setFromY(int fromY) {
        this.fromY = fromY;
    }

    public int getToY() {
        return toY;
    }

    public void setToY(int toY) {
        this.toY = toY;
    }

    public Plant getPlant() {
        return plant;
    }

    public void setPlant(Plant plant) {
        this.plant = plant;
    }

    public Garden getGarden() {
        return garden;
    }

    public void setGarden(Garden garden) {
        this.garden = garden;
    }

    @Override
    public String toString() {
        return "<<Plot>> from x"+fromX+" y"+fromY+" to x"+toX+" y"+toY+" \n"+ plant;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        Plot plot = null;
        try{
            plot = (Plot) obj;
        }catch (Exception e){
            return false;
        }
        return plot.toY == toY && plot.toX == toX && plot.fromY == fromY && fromX == plot.fromX && plot.getGarden().equals(garden);
    }
}
