package model;

public class OutputGardener {
    public int id;
    public String name;
    public String username;
    public String password;
    public int teamId;

    public OutputGardener() {
    }

    public OutputGardener(int id, String name, String username, String password, int teamId) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.teamId = teamId;
    }

}
