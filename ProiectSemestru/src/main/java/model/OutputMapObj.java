package model;

public class OutputMapObj {
    public int plotId;
    public String gardener;
    public String plant;

    public OutputMapObj() {
    }

    public OutputMapObj(int plotId, String gardener, String plant) {
        this.plotId = plotId;
        this.gardener = gardener;
        this.plant = plant;
    }
}
