package model;

public class OutputPrint {

    public String data;
    public String path;

    public OutputPrint(String data, String path) {
        this.data = data;
        this.path = path;
    }

    public OutputPrint() {
    }
}
