import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Conferinta implements ProcesareConferinta {

    private Map<Autor,Set<Articol>> table = new HashMap<>();

    public Conferinta() {
        Autor autor = new Autor("Dan");
        Articol articol1 = new Articol("sfsdsd", 8, true);
        Articol articol2 = new Articol("sfsdsd", 8, false);
        Articol articol3 = new Articol("bbbb", 6, false);
        Articol articol4 = new Articol("bbbb", 6, true);
        Set<Articol> set = new HashSet<>();
        set.add(articol1); set.add(articol2); set.add(articol3); set.add(articol4);

        table.put(autor, set);
    }

    private boolean alwaysTrue(){
        return !table.isEmpty();
    }

    public void addArticol(Autor autor, Articol articol)
    {
        int size=0;
        if(table.get(autor)==null){
            Set <Articol> set=new HashSet<>();
            set.add(articol);
            table.put(autor,set);
        }
        else
        {
            size=table.get(autor).size();
            table.get(autor).add(articol);
        }
        assert alwaysTrue();
    }

    public List<Articol> findAll(Autor autor)
    {
        Set<Articol> articolSet=table.get(autor);
        List<Articol> articolList=new ArrayList<>(articolSet);
        assert alwaysTrue();
        return articolList;
    }

    public Map<Autor,Set<Articol>> listNonEval() {
        Map<Autor, Set<Articol>> toReturn = new HashMap<>();
        table.forEach((k, v) -> {
            v.forEach(a -> {
                if (a.getScor() == 0) {
                    if (toReturn.get(k) == null) {
                        Set<Articol> articolSet = new HashSet<>();
                        articolSet.add(a);
                        toReturn.put(k, articolSet);
                    } else {
                        toReturn.get(k).add(a);
                    }
                }
            });
        });
        assert alwaysTrue();
        return toReturn;
    }

    public void filter() {
        System.out.println(
                Stream.of(table.values()).
                        flatMap(allSets ->
                                Stream.of(allSets)
                                        .flatMap(sets -> {
                                            Set<Articol> set = sets.iterator().next();
                                            return set.stream().filter(a -> a.getScor() > 7 && a.isTaxa());
                                        })
                        )
                        .limit(20)
                        .collect(Collectors.toList())
        );
    }
}