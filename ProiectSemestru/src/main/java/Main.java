import model.*;
import server.repos.*;

public class Main {
    private static GardenerRepo gardenerRepo = new GardenerRepo();
    private static PlantRepo plantDAO = new PlantRepo();
    private static EmployeeRepo employeeRepo = new EmployeeRepo();
    private static TeamRepo teamRepo = new TeamRepo();
    private static SupervisorRepo supervisorDAO = new SupervisorRepo();
    private static PlotRepo plotDAO = new PlotRepo();
    private static GardenRepo gardenRepo = new GardenRepo();

    public static void main(String[] args) {
        update();
        //LoginManagement loginManagement = new LoginManagement();
        //LoginManagement.showMainPanelWindow();
    }

    private static void update(){
        Garden garden = gardenRepo.getOne(11);
        Plot p1 = new Plot(garden, 0,2,0,2);
        Plot p2 = new Plot(garden, 3,4,0,2);
        Plot p3 = new Plot(garden, 0, 2, 3,5);
        Plot p4 = new Plot(garden, 0, 2, 6, 7);
        Plot p5 = new Plot(garden, 3, 4, 3,7);
        plotDAO.save(p1);
        plotDAO.save(p2);
        plotDAO.save(p3);
        plotDAO.save(p4);
        plotDAO.save(p5);
    }

    private static void initDB(){

        Plant plant1 = new Plant("cereal","rice","3x10",4,3,10);
        Plant plant2 = new Plant("flower", "tulip", "30cm",10,3, 10);
        Plant plant3 = new Plant("flower", "rose", "50 cm", 5, 8, 10);

        plantDAO.save(plant1);
        plantDAO.save(plant2);
        plantDAO.save(plant3);

        Employee e = new Employee("gAna","girl1", "Ana");
        Employee e2 = new Employee("gMircea", "b2", "Mircea");
        Employee e3 = new Employee("sSimona", "123", "Simona");
        employeeRepo.save(e);
        employeeRepo.save(e2);
        employeeRepo.save(e3);

        Team t = new Team();
        teamRepo.save(t);

        Gardener g1 = new Gardener(0,e,t);
        gardenerRepo.save(g1);

        Gardener g2 = new Gardener(0,e2,t);
        gardenerRepo.save(g2);

        Supervisor s = new Supervisor(e3, t);
        supervisorDAO.save(s);


        Garden garden = new Garden(5,8, t);
        gardenRepo.save(garden);

        Plot p1 = new Plot(garden, 0,2,0,2);
        Plot p2 = new Plot(garden, 3,4,0,2);
        Plot p3 = new Plot(garden, 0, 2, 3,5);
        Plot p4 = new Plot(garden, 0, 2, 6, 7);
        Plot p5 = new Plot(garden, 3, 4, 3,7);
        plotDAO.save(p1);
        plotDAO.save(p2);
        plotDAO.save(p3);
        plotDAO.save(p4);
        plotDAO.save(p5);

    }
}
