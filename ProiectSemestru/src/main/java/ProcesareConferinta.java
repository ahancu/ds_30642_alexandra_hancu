import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ProcesareConferinta {

    /**
     *@param autor!= null
     *@param articol != null
    */
    public void addArticol(Autor autor,Articol articol);

    /**
     * @param autor!= null
     * @return articolList!=null
     */
    List<Articol> findAll(Autor autor);

    /**
     * @return autor!= null
     * @return articol!=null
     */
    Map<Autor,Set<Articol>> listNonEval();
}
