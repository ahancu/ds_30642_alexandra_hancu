package client;

public class PrinterFactory {

    public IPrinter getPrinter(boolean isHtml){
        if(isHtml){
            return new HtmlPrinter();
        }
        else {
            return new PdfPrinter();
        }
    }

}
