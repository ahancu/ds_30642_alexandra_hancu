package client;

public interface IPrinter {

    public boolean print(String data, String path);
}
