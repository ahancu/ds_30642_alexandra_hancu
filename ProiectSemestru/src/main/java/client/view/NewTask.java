package client.view;

import javax.swing.*;

public class NewTask extends JFrame {
    private JPanel newTask;

    private JComboBox gardenerCB;
    private JComboBox plotCB;
    private JTextField timeTF;
    private JTextField rewardTF;
    private JButton saveButton;

    public NewTask(){
        setSize(500,500);
        setContentPane(newTask);
        setLocationRelativeTo(null);

    }

    public JComboBox getGardenerCB() {
        return gardenerCB;
    }

    public JComboBox getPlotCB() {
        return plotCB;
    }

    public JTextField getTimeTF() {
        return timeTF;
    }

    public JTextField getRewardTF() {
        return rewardTF;
    }

    public JButton getSaveButton() {
        return saveButton;
    }
}
