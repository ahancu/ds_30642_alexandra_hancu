package client.view;

import client.Client;
import model.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class NewTaskManagement {
    private NewTask newTask;

    private JComboBox gardenerCB;
    private JComboBox plotCB;
    private JTextField timeTF;
    private JTextField rewardTF;
    private JButton saveButton;

    private Garden garden;
    private List<Plot> plots;
    private Team team;
    private List<Gardener> teamMembers;
    Client client;

    public NewTaskManagement(Client client, Team team){
        this.client = client;
        this.team = team;
        initComponents();
        initListeners();
    }

    private void initComponents() {
        garden = client.getGarden(team);
        newTask = new NewTask();
        gardenerCB = newTask.getGardenerCB();
        teamMembers = client.getTeamMembers(team);
        for(Gardener gardener: teamMembers){
            gardenerCB.addItem(gardener);
        }
        plotCB = newTask.getPlotCB();
        plots = client.getPlots(garden);//plantService.getPlants();
        for (Plot p: plots ) {
            plotCB.addItem(p);
        }
        timeTF = newTask.getTimeTF();
        rewardTF = newTask.getRewardTF();
        saveButton = newTask.getSaveButton();

    }

    private void initListeners() {
        saveButton.addActionListener(new SaveNewTaskListener());
    }

    public void showMainPanelWindow(boolean visibility) {
        newTask.setVisible(visibility);
    }

    private class SaveNewTaskListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int time = 0;
            try {
                time = Integer.parseInt(timeTF.getText());
            } catch (Exception exc){
                System.out.println("WRONG INPUT 4 TIME");
            }
            if( time != 0){
                float reward = 0;
                try {
                    reward = Float.parseFloat(rewardTF.getText());
                } catch (Exception exc){
                    System.out.println("WRONG INPUT 4 REWARD");
                }
                if(reward != 0){
                    client.newTask((Plot)plotCB.getSelectedItem(), time, reward, (Gardener)gardenerCB.getSelectedItem());
                }
            }
        }
    }
}
