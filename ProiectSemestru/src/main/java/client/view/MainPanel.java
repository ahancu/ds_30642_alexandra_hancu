package client.view;

import javax.swing.*;
import java.awt.*;

public class MainPanel extends JFrame{
    private JPanel mainPanel;
    private JTextField usernameTF;
    private JPasswordField passwordPF;
    private JLabel usernameL;
    private JLabel passwordL;
    private JButton loginB;
    private JButton adminB;

    public MainPanel() throws HeadlessException {
        setSize(500,500);
        setContentPane(mainPanel);
        setLocationRelativeTo(null);
    }

    public JTextField getUsernameTF() {
        return usernameTF;
    }

    public JPasswordField getPasswordPF() {
        return passwordPF;
    }

    public JLabel getUsernameL() {
        return usernameL;
    }

    public JLabel getPasswordL() {
        return passwordL;
    }

    public JButton getLoginB() {
        return loginB;
    }

    public JButton getAdminB() {
        return adminB;
    }
}
