package client.view;

import client.Client;
import client.IPrinter;
import client.PrinterFactory;
import model.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;


public class SupervisorViewManagement {

    Supervisor supervisor;

    private SupervisorPanel supervisorPanel;
    private JPanel plants;
    private JPanel gardener;
    private JButton newGardenerButton;
    private JButton newPlantButton;
    private JButton seePlantListButton;
    private JButton plantInfoBtn;
    private JTextField plantIdTF;
    private JButton seeTeamMembersButton;
    private JTextField nameTF;
    private JButton logOutButton;
    private JTextField gardenerId;
    private JButton gardenerInfoBtn;
    private JTextArea outputTA;
    private JComboBox reportTypeCB;
    private JButton saveButton;
    private PrinterFactory printerFactory;
    private Client client;
    private JButton giveTasksButton;
    private JTextField notificationsTF;
    private JButton reviewRefillRequestsButton;

    public SupervisorViewManagement(Supervisor supervisor, PrinterFactory printerFactory, Client client) {
        this.client = client;
        this.supervisor = supervisor;
        this.printerFactory =printerFactory;
        initComponents();
        initListeners();
    }

    private void initComponents() {
        supervisorPanel = new SupervisorPanel();
        plants = supervisorPanel.getPlants();
        gardener = supervisorPanel.getGardener();
        newGardenerButton = supervisorPanel.getNewGardenerButton();
        newPlantButton = supervisorPanel.getNewPlantButton();
        seePlantListButton = supervisorPanel.getSeePlantListButton();
        plantInfoBtn = supervisorPanel.getPlantInfoBtn();
        plantIdTF = supervisorPanel.getPlantIdTF();
        seeTeamMembersButton = supervisorPanel.getSeeTeamMembersButton();
        nameTF = supervisorPanel.getNameTF();
        nameTF.setText(supervisor.getEmployee().getName());
        logOutButton = supervisorPanel.getLogOutButton();
        gardenerId = supervisorPanel.getGardenerId();
        gardenerInfoBtn = supervisorPanel.getGardenerInfoBtn();
        outputTA = supervisorPanel.getOutputTA();
        reportTypeCB = supervisorPanel.getReportTypeCB();
        saveButton = supervisorPanel.getSaveButton();
        giveTasksButton = supervisorPanel.getGiveTasksButton();
        notificationsTF = supervisorPanel.getNotificationsTF();
        reviewRefillRequestsButton = supervisorPanel.getReviewRefillRequestsButton();
    }

    private void initListeners() {
        newGardenerButton.addActionListener(new NewGardenerListener());
        newPlantButton.addActionListener(new NewPlantListener());
        seePlantListButton.addActionListener(new SeePlantListListener());
        plantInfoBtn.addActionListener(new PlantInfoListener());
        seeTeamMembersButton.addActionListener(new TeamMembersListener());
        logOutButton.addActionListener(new LogOutListener());
        gardenerInfoBtn.addActionListener(new GardenerInfoListener());
        saveButton.addActionListener(new SaveReportListener());
        giveTasksButton.addActionListener(new GiveTasksListener());
        reviewRefillRequestsButton.addActionListener(new ReviewRefillRequestsListener());
    }

    public void setVisibility(boolean b) {
        supervisorPanel.setVisible(b);
    }

    private class NewGardenerListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            NewGardenerManagement newGardenerManagement = new NewGardenerManagement(client);
            newGardenerManagement.setVisibility(true);
        }
    }

    private class NewPlantListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            NewPlantManagement newPlantManagement =new NewPlantManagement(client);
            newPlantManagement.setVisibility(true);
        }
    }

    private class SeePlantListListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            outputTA.setText(client.getPlantList().toString());//supervisorService.getPlantList().toString());
        }
    }

    private class PlantInfoListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            Plant p = null;
            int id = 0;
            try{
                id = Integer.parseInt(plantIdTF.getText());
            }
            catch (Exception exc){
                outputTA.setText("WRONG INPUT IN FIELD PLANT ID");
            }
            if(id!=0)
                p = client.getPlant(id);//supervisorService.getPlant(plantIdTF.getText());
            if(p==null)
                outputTA.setText("WRONG INPUT IN FIELD PLANT ID");
            else
                outputTA.setText(p.toString());
        }
    }

    private class TeamMembersListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            Team team = supervisor.getTeam();
            List<Gardener> teamList = client.getTeamMembers(team); //supervisorService.getTeamMembers(team);
            outputTA.setText(teamList.toString());
        }
    }

    private class LogOutListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            setVisibility(false);
            LoginManagement loginManagement = new LoginManagement(client);
            loginManagement.showMainPanelWindow();
        }

    }

    private class GardenerInfoListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int id = 0;
            try {
                id = Integer.parseInt(gardenerId.getText());
            }catch (Exception exc){
                outputTA.setText("WRONG INPUT IN GARDENER ID FIELD!");
                return;
            }
            Gardener gardener = client.getGardenerInfo(id); //supervisorService.getGardenerInfo(id);
            outputTA.setText(gardener.toString());
        }
    }

    private class SaveReportListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            boolean isHtml = reportTypeCB.getSelectedItem().toString().compareTo("html")==0;
            FileChooser fileChooser = new FileChooser(supervisorPanel);
            String path = fileChooser.getLocation().getPath();
            IPrinter printer = printerFactory.getPrinter(isHtml);
            printer.print(client.getPlantList().toString(), path);
        }
    }

    private class GiveTasksListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            NewTaskManagement newTaskManagement = new NewTaskManagement(client, supervisor.getTeam());
            newTaskManagement.showMainPanelWindow(true);
        }
    }

    private class ReviewRefillRequestsListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            List<RefillRequest> requests = supervisor.getRefillList();

            String output = "";
            for (RefillRequest refillRequest: requests) {
                output += refillRequest.toString();
            }
            outputTA.setText(output);
        }
    }
}