package client.view;

import client.Client;
import model.Plant;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NewPlantManagement {
    private NewPlant newPlant;
    private JTextField nameTF;
    private JTextField typeTF;
    private JTextField dimTF;
    private JTextField waterAmTF;
    private JTextField waterInTF;
    private JButton createBtn;
    private JTextField messageBox;

    private Client client;

    public NewPlantManagement(Client client){
        this.client = client;
        initComponents();
        initListeners();
    }

    private void initComponents() {
        newPlant = new NewPlant();
        nameTF = newPlant.getNameTF();
        typeTF = newPlant.getTypeTF();
        dimTF = newPlant.getDimTF();
        waterAmTF = newPlant.getWaterAmTF();
        waterInTF = newPlant.getWaterInTF();
        createBtn = newPlant.getCreateBtn();
        messageBox = newPlant.getMessageBox();
    }

    public void setVisibility(boolean b){
        newPlant.setVisible(b);
    }

    private void initListeners() {
        createBtn.addActionListener( new CreateListener());
    }

    private class CreateListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            Plant p = new Plant(typeTF.getText(), nameTF.getText(), dimTF.getText(),
                    Integer.parseInt(waterInTF.getText()), Integer.parseInt(waterAmTF.getText()), 10);
            if(client.newPlant(p)){//supervisorService.newPlant(p)){
                   messageBox.setText("plant saved!\n"+p.toString());
            }
        }
    }
}
