package client.view;

import javax.swing.*;
import java.awt.*;

public class MapPanel extends JFrame{
    private JPanel mapPanel;
    private JTable map;
    private JTextField messageBox;
    private JScrollPane scrollPane;

    public MapPanel(Object tableData[][],int nx, int ny) throws HeadlessException {
        mapPanel = new JPanel();
        messageBox = new JTextField();

        setSize(500,500);

        String[] columnNames = new String[nx];
        for(int i=0; i<nx; i++){
            columnNames[i] = " ";
        }
        map = new JTable(tableData, columnNames);
        scrollPane = new JScrollPane(map);
        map.setFillsViewportHeight(true);
        map.setPreferredScrollableViewportSize(map.getPreferredSize());
        map.setTableHeader(null);

        mapPanel.add(scrollPane);

        setContentPane(mapPanel);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public JTable getMap() {
        return map;
    }

    public JTextField getMessageBox() {
        return messageBox;
    }

    public JScrollPane getScrollPane() {
        return scrollPane;
    }
}
