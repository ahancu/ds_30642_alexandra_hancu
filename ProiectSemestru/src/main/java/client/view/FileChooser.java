package client.view;

import javax.swing.*;
import java.io.File;

public class FileChooser {

        File location;

        public FileChooser(JFrame parent){

            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new java.io.File("."));
            chooser.setDialogTitle("choose");
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            //
            // disable the "All files" option.
            //
            chooser.setAcceptAllFileFilterUsed(false);
            //
            if (chooser.showOpenDialog(parent) == JFileChooser.APPROVE_OPTION) {
                System.out.println("Selected location : " +  chooser.getSelectedFile());
                location = chooser.getSelectedFile();
            }
            else {
                System.out.println("No Selection ");
            }
        }

    public File getLocation() {
        return location;
    }
}
