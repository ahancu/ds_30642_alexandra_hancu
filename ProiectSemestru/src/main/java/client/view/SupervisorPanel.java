package client.view;

import javax.swing.*;
import java.awt.*;

public class SupervisorPanel  extends JFrame{
    private JPanel supervisorPanel;
    private JPanel plants;
    private JPanel gardener;
    private JButton newGardenerButton;
    private JButton newPlantButton;
    private JButton seePlantListButton;
    private JButton plantInfoBtn;
    private JTextField plantIdTF;
    private JButton seeTeamMembersButton;
    private JTextField nameTF;
    private JButton logOutButton;
    private JTextField gardenerId;
    private JButton gardenerInfoBtn;
    private JTextArea outputTA;
    private JComboBox reportTypeCB;
    private JButton saveButton;
    private JButton giveTasksButton;
    private JTextField notificationsTF;
    private JButton reviewRefillRequestsButton;

    public SupervisorPanel() throws HeadlessException {
        setSize(1300,500);
        setContentPane(supervisorPanel);
        setLocationRelativeTo(null);
    }

    public JButton getReviewRefillRequestsButton() {
        return reviewRefillRequestsButton;
    }

    public JPanel getPlants() {
        return plants;
    }

    public JPanel getGardener() {
        return gardener;
    }

    public JButton getNewGardenerButton() {
        return newGardenerButton;
    }

    public JButton getNewPlantButton() {
        return newPlantButton;
    }

    public JButton getSeePlantListButton() {
        return seePlantListButton;
    }

    public JButton getPlantInfoBtn() {
        return plantInfoBtn;
    }

    public JTextField getPlantIdTF() {
        return plantIdTF;
    }

    public JButton getSeeTeamMembersButton() {
        return seeTeamMembersButton;
    }

    public JTextField getNameTF() {
        return nameTF;
    }

    public JButton getLogOutButton() {
        return logOutButton;
    }

    public JTextField getGardenerId() {
        return gardenerId;
    }

    public JButton getGardenerInfoBtn() {
        return gardenerInfoBtn;
    }

    public JTextArea getOutputTA() {
        return outputTA;
    }

    public JComboBox getReportTypeCB() {
        return reportTypeCB;
    }

    public JButton getSaveButton() {
        return saveButton;
    }

    public JButton getGiveTasksButton() {
        return giveTasksButton;
    }

    public JTextField getNotificationsTF() {
        return notificationsTF;
    }
}
