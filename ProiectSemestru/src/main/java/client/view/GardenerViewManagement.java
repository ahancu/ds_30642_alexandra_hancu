package client.view;

import client.Client;
import model.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class GardenerViewManagement {
    private GardenerPanel gardenerPanel;
    private JLabel welcome;
    private JTextField textField1;
    private JButton seeMapButton;
    private JComboBox plantCB;
    private JComboBox plotCB;
    private JButton logOutButton;
    private JButton saveButton;


    private Gardener gardener;
    private Garden garden;
    private List<Plant> plants;
    private List<Plot> plots;

    private Client client;

    public GardenerViewManagement(Gardener gardener, Client client){
        this.gardener = gardener;
        this.client = client;
        initComponents();
        initListeners();
    }

    private void initComponents() {
        //garden = gardenerService.getGarden(gardener);
        garden = client.getGarden(11);//gardenService.getGarden(11);
        gardenerPanel = new GardenerPanel();
        welcome = gardenerPanel.getWelcome();
        textField1 = gardenerPanel.getMessageBox();
        textField1.setText(gardener.getEmployee().getName());
        seeMapButton = gardenerPanel.getSeeMapButton();
        plantCB = gardenerPanel.getPlantCB();
        plants = client.getPlantList();//plantService.getPlants();
        for (Plant p: plants ) {
            plantCB.addItem(p);
        }
        logOutButton = gardenerPanel.getLogOutButton();
        plotCB = gardenerPanel.getPlotCB();
        for (Plot p: client.getPlots(garden)){//mapService.getPlots(garden)){
            plotCB.addItem(p);
        }
        saveButton = gardenerPanel.getSaveButton();

    }

    private void initListeners(){

        seeMapButton.addActionListener(new SeeMapActionListener());
        saveButton.addActionListener( new SavePlantListener());
        logOutButton.addActionListener( new LogOutListener());
    }

    public void setVisibility(boolean b) {
        gardenerPanel.setVisible(b);
    }

    private class SeeMapActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            garden = client.getGarden(11);
            MapManagement mapManagement = new MapManagement(garden, client);
            mapManagement.setVisibility(true);
        }
    }

    private class SavePlantListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Plant plant = (Plant)plantCB.getSelectedItem();
            Plot plot = (Plot)plotCB.getSelectedItem();
            boolean response = client.addPlant(plot, plant);//plantService.addPlant((Plot)plotCB.getSelectedItem(), (Plant)plantCB.getSelectedItem());
            if(response == true && plant!= null){
                textField1.setText("plant"+ plant + "added to plot" + plot);
                plant.decreaseNoSeeds();
                if (plant.getNoSeeds()== 0 ){
                   Supervisor s = client.getSupervisor(gardener.getEmployee());
                   RefillRequest request = client.getRequestByGardener(gardener);
                   if(request == null){
                       List<Plant> empty = new ArrayList<>();
                       empty.add(plant);
                       request = new RefillRequest(gardener, empty);
                       request.setSupervisor(s);
                       client.newRequest(request);
                   }else {
                       List<Plant> plants = request.getPlants();
                       plants.add(plant);
                       request.setPlants(plants);
                       client.updateRequest(request);
                   }
                   s.addToRefillList(request);
                   client.updateSupervisor(s);
                }
                client.updatePlant(plant);
                textField1.setText(textField1.getText()+ "\n plant updated. hass less seeds");
            }
        }
    }
    private class LogOutListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            setVisibility(false);
            LoginManagement loginManagement = new LoginManagement(client);
            loginManagement.showMainPanelWindow();
        }

    }
}
