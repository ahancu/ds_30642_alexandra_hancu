package client.view;

import javax.swing.*;
import java.awt.*;

public class GardenerPanel extends JFrame{
    private JPanel gardenerPanel;
    private JLabel welcome;
    private JTextField messageBox;
    private JButton seeMapButton;
    private JComboBox plantCB;
    private JComboBox plotCB;
    private JButton logOutButton;
    private JButton saveButton;

    public GardenerPanel() throws HeadlessException {
        setSize(1300,500);
        setContentPane(gardenerPanel);
        setLocationRelativeTo(null);
    }

    public JPanel getGardenerPanel() {
        return gardenerPanel;
    }

    public JLabel getWelcome() {
        return welcome;
    }

    public JTextField getMessageBox() {
        return messageBox;
    }

    public JComboBox getPlantCB() {
        return plantCB;
    }

    public JComboBox getPlotCB() {
        return plotCB;
    }

    public JButton getLogOutButton() {
        return logOutButton;
    }

    public JButton getSaveButton() {
        return saveButton;
    }

    public JButton getSeeMapButton() {
        return seeMapButton;
    }
}
