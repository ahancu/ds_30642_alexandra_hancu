package client.view;

import client.Client;
import model.Garden;
import model.Plot;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import java.util.List;

public class MapManagement
{
    private Garden garden;
    private List<Plot>plots;
    private Object[][] mapData;

    private MapPanel mapPanel;
    private JTable map;
    private JTextField messageBox;
    private JScrollPane scrollPane;

    private Client client;

    public MapManagement(Garden garden, Client client){
        this.garden = garden;
        this.client = client;
        initComponents();
        initListeners();

    }

    private void refreshData(){
        garden = client.getGarden(garden.getId());
        plots=client.getPlots(garden);//mapService.getPlots(garden);
        mapData=client.makeGardenMatrix(garden, plots); //mapService.makeGardenMatrix(garden,plots);
    }

    public void initComponents(){
        refreshData();
        mapPanel = new MapPanel(mapData, garden.getWidth(), garden.getLength());
        map = mapPanel.getMap();
        messageBox = mapPanel.getMessageBox();
        scrollPane = mapPanel.getScrollPane();
    }

    public void initListeners() {
        ListSelectionModel cellSelectionModel = map.getSelectionModel();
        cellSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        cellSelectionModel.addListSelectionListener(new ListSelectionListener());

    }

    public void setVisibility(boolean b) {
        refreshData();
        mapPanel.setVisible(b);
    }

    private class ListSelectionListener implements javax.swing.event.ListSelectionListener {
        @Override
        public void valueChanged(ListSelectionEvent e) {
            String selectedData = null;

            int[] selectedRow = map.getSelectedRows();
            int[] selectedColumns = map.getSelectedColumns();

            for (int i = 0; i < selectedRow.length; i++) {
                for (int j = 0; j < selectedColumns.length; j++) {
                    selectedData = (String) map.getValueAt(selectedRow[i], selectedColumns[j]);
                }
            }
            System.out.println("Selected: " + selectedData);
        }
    }
}
