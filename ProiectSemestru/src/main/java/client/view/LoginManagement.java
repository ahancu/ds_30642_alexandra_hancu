package client.view;

import client.Client;
import client.PrinterFactory;
import model.Employee;
import model.Gardener;
import model.Supervisor;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginManagement {

    private static MainPanel mainPanel;
    private JTextField usernameTF;
    private JPasswordField passwordPF;
    private JLabel usernameL;
    private JLabel passwordL;
    private JButton loginB;
    private JButton adminB;
    private Employee employee;
    private Client client;

    public LoginManagement(Client client) {
        this.client = client;
        initComponents();
        initListeners();
    }

    public void showMainPanelWindow(){
        mainPanel.setVisible(true);
    }

    private void initComponents() {
        mainPanel = new MainPanel();

        usernameTF = mainPanel.getUsernameTF();
        usernameTF.setText("");
        usernameL = mainPanel.getUsernameL();
        passwordPF = mainPanel.getPasswordPF();
        passwordPF.setText("");
        passwordL = mainPanel.getPasswordL();
        loginB = mainPanel.getLoginB();
        adminB = mainPanel.getAdminB();
    }

    private void initListeners() {
        loginB.addActionListener(new LoginBListener());
        adminB.addActionListener( new AdminBListener());
    }

    private class LoginBListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("in login management");
            boolean loggedIn = client.login(usernameTF.getText(), new String(passwordPF.getPassword()));//loginService.login(usernameTF.getText(), new String(passwordPF.getPassword()));
            if(loggedIn) {
                System.out.println("you are now logged in");
                client.setUsername(usernameTF.getText());
                employee = client.getEmployee(usernameTF.getText());//loginService.getEmployee(usernameTF.getText());
                Gardener gardener = client.getGardener(employee);
                if(gardener!= null) {
                    GardenerViewManagement gardenerViewManagement = new GardenerViewManagement(gardener, client);
                    gardenerViewManagement.setVisibility(true);
                    mainPanel.setVisible(false);
                }
                else {
                    Supervisor supervisor = client.getSupervisor(employee);
                    if(supervisor!= null) {
                        SupervisorViewManagement supervisorViewManagement = new SupervisorViewManagement(supervisor, new PrinterFactory(),client);
                        supervisorViewManagement.setVisibility(true);
                        mainPanel.setVisible(false);
                    }
                    else {
                        System.out.println("employee not connected to actual user");
                        usernameTF.setText("");
                        passwordPF.setText("");
                    }
                }
            }
            else{
                System.out.println("log in failed. try again.");
                usernameTF.setText("");
                passwordPF.setText("");
            }
        }
    }

    private class AdminBListener implements ActionListener  {
        @Override
        public void actionPerformed(ActionEvent e) {
//            System.out.println("welcome back admin");
//            AdminPanelController employeePanelController = new AdminPanelController();
//            employeePanelController.showAdminPanelWindow();
//            mainPanel.setVisible(false);
        }
    }
}
