package client.view;

import javax.swing.*;
import java.awt.*;

public class NewGardener extends JFrame{
    private JTextField usernameTF;
    private JTextField passTF;
    private JTextField nameTF;
    private JButton saveBtn;
    private JPanel newGardener;
    private JTextField messageBox;
    private JRadioButton newTeamRB;
    private JRadioButton existingTeamRB;
    private JTextField teamId;

    public NewGardener() throws HeadlessException {
        setSize(500,500);
        setContentPane(newGardener);
        setLocationRelativeTo(null);
    }

    public JTextField getUsernameTF() {
        return usernameTF;
    }

    public JTextField getPassTF() {
        return passTF;
    }

    public JTextField getNameTF() {
        return nameTF;
    }

    public JButton getSaveBtn() {
        return saveBtn;
    }

    public JTextField getMessageBox() {
        return messageBox;
    }

    public JRadioButton getNewTeamRB() {
        return newTeamRB;
    }

    public JRadioButton getExistingTeamRB() {
        return existingTeamRB;
    }

    public JTextField getTeamId() {
        return teamId;
    }
}
