package client.view;

import javax.swing.*;
import java.awt.*;

public class NewPlant extends JFrame{
    private JPanel newPlant;
    private JTextField nameTF;
    private JTextField typeTF;
    private JTextField dimTF;
    private JTextField waterAmTF;
    private JTextField waterInTF;
    private JButton createBtn;
    private JTextField messageBox;

    public NewPlant() throws HeadlessException {
        setSize(500,500);
        setContentPane(newPlant);
        setLocationRelativeTo(null);
    }

    public JTextField getNameTF() {
        return nameTF;
    }

    public JTextField getTypeTF() {
        return typeTF;
    }

    public JTextField getDimTF() {
        return dimTF;
    }

    public JTextField getWaterAmTF() {
        return waterAmTF;
    }

    public JTextField getWaterInTF() {
        return waterInTF;
    }

    public JButton getCreateBtn() {
        return createBtn;
    }

    public JTextField getMessageBox() {
        return messageBox;
    }
}
