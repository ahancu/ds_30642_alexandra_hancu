package client.view;

import client.Client;
import model.Employee;
import model.Gardener;
import model.Team;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NewGardenerManagement {
    private JTextField usernameTF;
    private JTextField passPF;
    private JTextField nameTF;
    private JButton saveBtn;
    private NewGardener newGardener;
    private JTextField messageBox;
    private JRadioButton newTeamRB;
    private JRadioButton existingTeamRB;
    private JTextField teamId;
    private Client client;

    public NewGardenerManagement(Client client){
        this.client = client;
        initComponents();
        initListeners();
    }

    private void initComponents() {
        newGardener = new NewGardener();
        usernameTF = newGardener.getUsernameTF();
        passPF = newGardener.getPassTF();
        nameTF = newGardener.getNameTF();
        saveBtn = newGardener.getSaveBtn();
        messageBox = newGardener.getMessageBox();
        newTeamRB = newGardener.getNewTeamRB();
        existingTeamRB = newGardener.getExistingTeamRB();
        teamId = newGardener.getTeamId();
    }

    private void initListeners() {
        saveBtn.addActionListener( new SaveListener());
    }

    public void setVisibility(boolean b) {
        newGardener.setVisible(b);
    }

    private class SaveListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println(passPF.getText());
            Employee emp = new Employee(usernameTF.getText(), passPF.getText(), nameTF.getText());
            if(newTeamRB.isSelected()) {
                Team t = new Team();
                Gardener gardener = new Gardener(0, emp, t);
               if( client.newGardener(emp, t, gardener)){//supervisorService.newGardener(emp,t,gardener) ){
                   messageBox.setText("gardener saved"+ gardener);
               }
               else{
                   messageBox.setText("ERROR");
               }
            }
            else if (existingTeamRB.isSelected()){
                int id = Integer.parseInt(teamId.getText());
                Team t = client.getTeam(id);//supervisorService.getTeam(teamId.getText());
                if(t == null) {
                    messageBox.setText("ERROR");
                    return;
                }
                Gardener gardener = new Gardener(0,emp,t);
                if(client.newGardener(emp, gardener)){//supervisorService.newGardener(emp,gardener)){
                    messageBox.setText("gardener saved"+ gardener);
                }
                else{
                    messageBox.setText("ERROR");
                }
            }
        }
    }
}
