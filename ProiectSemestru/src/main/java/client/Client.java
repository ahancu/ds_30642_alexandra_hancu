package client;

import client.view.LoginManagement;
import model.*;
import model.Bus;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.time.Instant;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static model.MessageType.*;

public class Client extends Thread {

    private String username;
    private Socket socket;
    private Scanner console;
    private ObjectOutputStream outputStream;
    private ObjectInputStream inputStream;


    public Client (){
        try {
            socket = new Socket("127.0.0.1",1342);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT OPEN SOCKET");
        }
        console = new Scanner(System.in);
        try {
            inputStream = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT CREATE SCANER TO READ FROM SOCKET");
        }
        try {
            outputStream = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT CREATE PRINTER 4 SOCKET");
        }
        username = "";
    }

    private void check4Notification(){
        System.out.println("try to read notification");
        Bus msg = null;
        try {
            msg = (Bus) inputStream.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(Instant.now() + " Got from server: " + msg);
        readNotification(msg);
    }

    public static void main(String[] args) {
        Client c = new Client();
        c.start();
        System.out.println("new client");
        LoginManagement loginManagement = new LoginManagement(c);
        loginManagement.showMainPanelWindow();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private void readNotification(Bus bus){
        if(bus.getData().isEmpty())
            return;
        Supervisor supervisor = (Supervisor) bus.getData().get(0);
        String loggedIn = (String) bus.getData().get(1);
        if(loggedIn.isEmpty() || supervisor==null)
            return;
        System.out.println("NOTIFICATION\n\t" + supervisor + "\n\t"+username);
        if(supervisor.getEmployee().getUsername().compareTo(username)==0){
            System.out.println("SUPERV: "+  loggedIn + "is online");
            try {
                outputStream.writeObject(new Bus(notifcation, null, null));
            } catch (IOException e) {
                System.out.println("ERROR! CANNOT SEND NOTIF RECEIVED");
            }
        }

    }

    public synchronized Employee getEmployee(String username) {
        //send request
        try {
            List<Object> data = new ArrayList<>(); data.add(username);
            outputStream.writeObject(new Bus(getEmployee, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE STRING");
        }
        //get response
        Employee employee = null;
        Object response = null;
        try {
            response = inputStream.readObject();
            System.out.println("IN GET EMP. READ FROM SERVER:\n"+response);
            check4Notification();
            employee = (Employee) response;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return employee;
    }

    public synchronized boolean login(String username, String password) {
        //send request
        try {
            List<Object> data = new ArrayList<>();
            data.add(username);
            data.add(password);
            outputStream.writeObject(new Bus(login, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT SEND LOGIN REQUEST");
        }
        //readResponse
        boolean response =false;
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("ERROR! CANNOT PUT THREAD TO SLEEP");
        }
        try {

            response = inputStream.readBoolean();
            check4Notification();

        } catch (IOException e) {
            System.out.println(LocalTime.now()+"ERROR! CANNOT READ BOOLEAN");
        }
        System.out.println("???"+response);
        return response;
    }

    public synchronized boolean addPlant(Plot plot, Plant plant) {

        boolean response = false;
        //send request
        try {
            List<Object> data = new ArrayList<>();
            data.add(plot);
            data.add(plant);
            outputStream.writeObject(new Bus(addPlant, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT SEND ADD PLANT REQUEST");
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("ERROR! CANNOT PUT THREAD TO SLEEP");
        }
        //read response
        try {
            response = inputStream.readBoolean();
            check4Notification();///Era pe prima linie din metoda:-??
        } catch (IOException e) {
            System.out.println("ERROR!CANNOT READ ADD PLANT RESPONSE");
        }

        return response;
    }

    public synchronized Plant getPlant(int id) {

        Plant plant = null;

        //send req
        try {
            List<Object> data = new ArrayList<>();
            data.add(id);
            outputStream.writeObject(new Bus(getPlant, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT SEND GET PLANT REQ");
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("ERROR! CANNOT PUT THREAD TO SLEEP");
        }

        //get response
        try {
            plant = (Plant)inputStream.readObject();
            check4Notification();
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT READ PLANT OBJ- IOE");
        } catch (ClassNotFoundException e) {
            System.out.println("ERROR! CANNOT READ PLANT OBJ - CNFE");
        }
        return plant;

    }


    public synchronized List<Gardener> getTeamMembers(Team team) {

        List<Gardener> gardeners = null;
        //send req
        try {
            List<Object> data = new ArrayList<>();
            data.add(team);
            outputStream.writeObject(new Bus(getTeamMembers, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE TEAM "+team);
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("ERROR! CANNOT PUT THREAD TO SLEEP");
        }
        //get response
        try {
            gardeners = (List<Gardener>) inputStream.readObject();
            check4Notification();

        } catch (IOException e) {
            System.out.println("ERROR! CANNOT READ LIST - IOE");
        } catch (ClassNotFoundException e) {
            System.out.println("ERROR! CANNOT READ LIST - CNFE");
        }

        return gardeners;
    }

    public synchronized Gardener getGardenerInfo(int id) {

        Gardener gardener = null;
        //send req
        try {
            List<Object> data = new ArrayList<>();
            data.add(id);
            outputStream.writeObject(new Bus(getGardenerInfo, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT SEND GET GARDENER INFO REQ");
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("ERROR! CANNOT PUT THREAD TO SLEEP");
        }
        //read response
        try {
            gardener =(Gardener) inputStream.readObject();
            check4Notification();
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT READ GARDENER - IOE");
        } catch (ClassNotFoundException e) {
            System.out.println("ERROR! CANNOT READ GARDENER - CNFE");
        }

        return gardener;
    }

    public synchronized List<Plant> getPlantList() {

        List<Plant> plants = null;
        // send req
        try {
            outputStream.writeObject(new Bus(getPlantList, null, null));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE REQ 4 get plants");
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("ERROR! CANNOT PUT THREAD TO SLEEP");
        }
        //read response
        try {
            plants = (List<Plant>) inputStream.readObject();
            check4Notification();
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT READ PLANT LIST - IOE");
        } catch (ClassNotFoundException e) {
            System.out.println("ERROR! CANNOT READ PLANT LIST - CNFE");
        }

        return plants;
    }

    public synchronized Garden getGarden(int id) {

        Garden garden = null;
        try {
            List<Object> data = new ArrayList<>();
            data.add(id);
            outputStream.writeObject(new Bus(getGarden, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE GARDEN ID "+ id);
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("ERROR! CANNOT PUT THREAD TO SLEEP");
        }
        try {
            garden =(Garden) inputStream.readObject();
            check4Notification();
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT READ GARDEN OBJ - IOE");
        } catch (ClassNotFoundException e) {
            System.out.println("ERROR! CANNOT READ GARDEN OBJ - CNFE");
        }
        return garden;
    }

    public synchronized List<Plot> getPlots(Garden garden) {

        List<Plot> plots = null;
        //send request
        try {
            List<Object> data = new ArrayList<>();
            data.add(garden);
            outputStream.writeObject(new Bus(getPlots, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT SEND GET PLOTS REQ");
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("ERROR! CANNOT PUT THREAD TO SLEEP");
        }
        //get response
        try {
            plots = (List<Plot>) inputStream.readObject();
            check4Notification();
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT READ PLOTS - IOE");
        } catch (ClassNotFoundException e) {
            System.out.println("ERROR! CANNOT READ PLOTS - CNFE");
        }

        return plots;
    }

    public synchronized Object[][] makeGardenMatrix(Garden garden, List<Plot> plots) {

        Object[][] mapData = null;
        try {

            List<Object> data = new ArrayList<>();
            data.add(garden);
            data.add(plots);
            outputStream.writeObject(new Bus(makeGardenMatrix, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE REQUEST DATA TO MAKE GARDEN MATRIX");
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("ERROR! CANNOT PUT THREAD TO SLEEP");
        }
        try {
            mapData = (Object[][])inputStream.readObject();
            check4Notification();
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT READ MAP DATA MATRIX - IOE");
        } catch (ClassNotFoundException e) {
            System.out.println("ERROR! CANNOT READ MAP DATA MATRIX - CNFE");
        }

        return mapData;
    }

    public synchronized boolean newPlant(Plant p) {

        try {

            List<Object> data = new ArrayList<>();
            data.add(p);
            outputStream.writeObject(new Bus(newPlant, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE NEW PLANT REQ");
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("ERROR! CANNOT PUT THREAD TO SLEEP");
        }
        boolean response = false;
        try {
            response = inputStream.readBoolean();
            check4Notification();
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT READ RESPONSE AFTER NEW PLANT");
        }
        return response;
    }

    public synchronized boolean newGardener(Employee emp, Team t, Gardener gardener) {

        try {

            List<Object> data = new ArrayList<>();
            data.add(emp);
            data.add(t);
            data.add(gardener);
            outputStream.writeObject(new Bus(newGardener3, null, data));
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean response = false;

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("ERROR! CANNOT PUT THREAD TO SLEEP");
        }
        try {
            response = inputStream.readBoolean();
            check4Notification();
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT READ RESPONSE AFTER NEW GARDENER- 3ARG");
        }
        return response;
    }

    public synchronized boolean newGardener(Employee emp, Gardener gardener) {

        try {
            List<Object> data = new ArrayList<>();
            data.add(emp);
            data.add(gardener);
            outputStream.writeObject(new Bus(newGardener2, null, data));
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean response = false;
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("ERROR! CANNOT PUT THREAD TO SLEEP");
        }
        try {
            response = inputStream.readBoolean();
            check4Notification();
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT READ RESPONSE AFTER NEW GARDENER- 2ARG");
        }
        return response;
    }

    public synchronized Team getTeam(int id) {

        try {
            List<Object> data = new ArrayList<>();
            data.add(id);
            outputStream.writeObject(new Bus(getTeam, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE GET TEAM REQ");
        }

        Team team = null;
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("ERROR! CANNOT PUT THREAD TO SLEEP");
        }
        try {
            team = (Team) inputStream.readObject();
            check4Notification();
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT READ TEAM - IOE");
        } catch (ClassNotFoundException e) {
            System.out.println("ERROR! CANNOT READ TEAM - CNFE");
        }

        return team;
    }

    public String getNotification() {

        return null;
    }

    public synchronized Garden getGarden(Team team) {
        try {
            List<Object> data = new ArrayList<>();
            data.add(team);
            outputStream.writeObject(new Bus(getGardenByTeam, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE GET TEAM REQ");
        }
        Garden garden = null;
        try {
            garden = (Garden) inputStream.readObject();
            check4Notification();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return garden;
    }

    public synchronized Task newTask(Plot plot, int time, float reward, Gardener gardener) {
        try {

            List<Object> data = new ArrayList<>();
            data.add(plot);
            data.add(time);
            data.add(reward);
            data.add(gardener);
            outputStream.writeObject(new Bus(newTask, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE NEW TASK REQ");
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("ERROR! CANNOT PUT THREAD TO SLEEP");
        }
        Task task = null;

        try {
            task= (Task) inputStream.readObject();
            check4Notification();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return task;
    }

    public synchronized Gardener getGardener(Employee employee) {
        try {
            List<Object> data = new ArrayList<>();
            data.add(employee);
            outputStream.writeObject(new Bus(getGardenerByEmployee, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE GET GARDENER REQ");
        }
        Gardener gardener = null;
        try {
            gardener = (Gardener) inputStream.readObject();
            check4Notification();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return gardener;
    }

    public synchronized Supervisor getSupervisor(Employee employee) {
        try {

            List<Object> data = new ArrayList<>();
            data.add(employee);
            outputStream.writeObject(new Bus(getSupervisor, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE NEW PLANT REQ");
        }

        Supervisor supervisor = null;
        try {
            supervisor= (Supervisor) inputStream.readObject();
            check4Notification();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return supervisor;
    }

    public synchronized Plant updatePlant(Plant plant) {
        try {
            List<Object> data = new ArrayList<>();
            data.add(plant);
            outputStream.writeObject(new Bus(updatePlant, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE GET GARDENER REQ");
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("ERROR! CANNOT PUT THREAD TO SLEEP");
        }
        //read response
        Plant response = null;
        try {
            response =(Plant) inputStream.readObject();
            check4Notification();
        } catch (IOException e) {
            System.out.println("ERROR!CANNOT READ ADD PLANT RESPONSE - IOE");
        } catch (ClassNotFoundException e) {
            System.out.println("ERROR!CANNOT READ ADD PLANT RESPONSE - CNFE");
        }
        return response;
    }

    public Supervisor updateSupervisor(Supervisor s) {
        try {
            List<Object> data = new ArrayList<>();
            data.add(s);
            outputStream.writeObject(new Bus(updateSupervisor, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE UPDATE SUPERVISOR REQ");
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("ERROR! CANNOT PUT THREAD TO SLEEP");
        }
        //read response
        Supervisor response = null;
        try {
            response =(Supervisor) inputStream.readObject();
            check4Notification();
        } catch (IOException e) {
            System.out.println("ERROR!CANNOT READ ADD PLANT RESPONSE - IOE");
        } catch (ClassNotFoundException e) {
            System.out.println("ERROR!CANNOT READ ADD PLANT RESPONSE - CNFE");
        }
        return response;
    }

    public RefillRequest getRequestByGardener(Gardener gardener) {
        try {
            List<Object> data = new ArrayList<>();
            data.add(gardener);
            outputStream.writeObject(new Bus(getRefillRequestByGardener, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE getRequestByGardener REQ");
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("ERROR! CANNOT PUT THREAD TO SLEEP");
        }
        //read response
        RefillRequest response = null;
        try {
            response =(RefillRequest) inputStream.readObject();
            check4Notification();
        } catch (IOException e) {
            System.out.println("ERROR!CANNOT READ getRequestByGardener RESPONSE - IOE");
        } catch (ClassNotFoundException e) {
            System.out.println("ERROR!CANNOT READ getRequestByGardener RESPONSE - CNFE");
        }
        return response;
    }

    public boolean newRequest(RefillRequest request) {
        try {
            List<Object> data = new ArrayList<>();
            data.add(request);
            outputStream.writeObject(new Bus(newRequest, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE newRequest REQ");
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("ERROR! CANNOT PUT THREAD TO SLEEP");
        }
        //read response
        boolean response = false;
        try {
            response = inputStream.readBoolean();
            check4Notification();
        } catch (IOException e) {
            System.out.println("ERROR!CANNOT READ newRequest RESPONSE - IOE");
        }
        return response;
    }

    public RefillRequest updateRequest(RefillRequest request) {
        try {
            List<Object> data = new ArrayList<>();
            data.add(request);
            outputStream.writeObject(new Bus(updateRequest, null, data));
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE UPDATE Request REQ");
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("ERROR! CANNOT PUT THREAD TO SLEEP");
        }
        //read response
        RefillRequest response = null;
        try {
            response =(RefillRequest) inputStream.readObject();
            check4Notification();
        } catch (IOException e) {
            System.out.println("ERROR!CANNOT READ update Request RESPONSE - IOE");
        } catch (ClassNotFoundException e) {
            System.out.println("ERROR!CANNOT READ update Request RESPONSE - CNFE");
        }
        return response;
    }
}
