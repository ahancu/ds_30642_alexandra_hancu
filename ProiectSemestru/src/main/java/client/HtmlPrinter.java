package client;

import client.IPrinter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

public class HtmlPrinter implements IPrinter {

    public boolean print(String data, String path){

        try {
            OutputStream htmlfile= new FileOutputStream(new File(path+"\\report.html"));
            PrintStream printhtml = new PrintStream(htmlfile);

            String htmlheader="<html><head>";
            htmlheader+="<title>Equivalent HTML</title>";
            htmlheader+="</head><body>";
            String htmlfooter="</body></html>";
            printhtml.println(htmlheader + data + htmlfooter);
            printhtml.close();
            htmlfile.close();

        }

        catch (Exception e) {
            System.out.println("ERROR CREATING HTML FILE");
            return false;
        }
        return true;
    }
}
