
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class Articol extends Observable {
    private String titlu;
    private int scor;
    private boolean taxa;

    private ArrayList<Observer> observers =new ArrayList<Observer>();

    public Articol(String titlu, int scor, boolean taxa) {
        this.titlu = titlu;
        this.scor = scor;
        this.taxa = taxa;
    }

    public void addObserver(Observer o)
    {
        observers.add(o);
    }
    public void removeObserver(Observer o)
    {
        observers.remove(o);
    }

    public void notifyObserver(){
        for(Observer observer: observers) {
            observer.update(this, titlu);
        }
    }

    public String getTitlu() {
        return titlu;
    }

    public void setTitlu(String titlu) {
        this.titlu = titlu;
    }

    public int getScor() {
        return scor;
    }

    public void setScor(int scor) {
        this.scor = scor;
    }

    public boolean isTaxa() {
        return taxa;
    }

    public void setTaxa(boolean taxa) {
        this.taxa = taxa;
    }

    public void assignScor(int scor){
        if(scor>0 && scor<=10){
            setScor(scor);
        }
        if (scor>=8){
            notifyObserver();
        }
    }

    @Override
    public String toString() {
        return "Articol{" +
                "titlu='" + titlu + '\'' +
                ", scor=" + scor +
                ", taxa=" + taxa +
                '}';
    }
}