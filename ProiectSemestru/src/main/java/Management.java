import java.util.Observable;
import java.util.Observer;

public class Management implements Observer {
    private int contor;
    public Management()
    {
        this.contor=0;
    }

    public int getContor()
    {
        return contor;
    }

    @Override
    public void update(Observable o, Object arg) {
        contor++;
    }
}