package server.services;

import model.OutputPlot;
import model.Plant;
import model.Plot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.repos.PlantRepo;
import server.repos.PlotRepo;

import java.util.List;

@Service
public class PlantService {
    private PlantRepo plantRepo;
    private PlotRepo plotRepo;

    @Autowired
    public PlantService(PlantRepo plantRepo, PlotRepo plotRepo) {
        this.plantRepo = plantRepo;
        this.plotRepo = plotRepo;
    }

    public List<Plant> getPlants(){
        return plantRepo.getAll();
    }

    public Plot addPlant(Plot plot, Plant plant) {
        plantRepo.getOne(plant.getId());
        plot.setPlant(plant);
        return plotRepo.update(plot.getId(), plot);
    }

    public Plot addPlant(int plantId, int plotId) {
        return addPlant(plotRepo.getOne(plotId), plantRepo.getOne(plantId));
    }

    public Plant updatePlant(Plant plant) {
        return plantRepo.update(plant.getId(), plant);
    }

    public OutputPlot wrap(Plot addPlant) {
        return new OutputPlot(addPlant.getId(),
                addPlant.getFromX(),
                addPlant.getToX(),
                addPlant.getFromY(),
                addPlant.getToY(),
                addPlant.getGarden().getId(),
                addPlant.getPlant().getId());
    }
}
