package server.services;

import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.repos.GardenRepo;
import server.repos.GardenerRepo;
import server.repos.PlotRepo;

import java.util.ArrayList;
import java.util.List;

@Service
public class MapService {

    private PlotRepo plotRepo ;
    private GardenRepo gardenRepo;
    private GardenerRepo gardenerRepo;

    @Autowired
    public MapService(PlotRepo plotRepo, GardenerRepo gardenerRepo, GardenRepo gardenRepo){
        this.plotRepo = plotRepo;
        this.gardenerRepo = gardenerRepo;
        this.gardenRepo = gardenRepo;
    }

    public List<Plot> getPlots(Garden garden){
        List<Plot> allPlots = plotRepo.getAll();
        if(allPlots.isEmpty())
            return null;
        List<Plot> plots = new ArrayList<>();
        for (Plot p : allPlots) {
            if(p.getGarden().equals(garden))
                plots.add(p);
        }
        return plots;
    }

    public OutputMapObj[][] makeMatrix(){
        Garden garden = gardenRepo.getOne(27);
        return wrap(makeGardenMatrix(garden, plotRepo.getAllFor(garden)), garden.getWidth(), garden.getLength());
    }

    private OutputMapObj[][] wrap(MapObj[][] initial, int width, int length) {
        OutputMapObj[][]  output = new OutputMapObj[width][length];
        for(int i = 0; i<width; i++) {
            for (int j = 0; j < length; j++) {
                MapObj obj = initial[i][j];
                output[i][j]= getOutputMapObj(obj);
            }
        }
        return output;
    }

    private OutputMapObj getOutputMapObj(MapObj obj) {
        if(obj.getGardener() != null && obj.getGardener().getEmployee() != null){
            if(obj.getPlant() != null){
                return new OutputMapObj(
                        obj.getPlotId(),
                        obj.getGardener().getEmployee().getName(),
                        obj.getPlant().getName());
            }
            return new OutputMapObj(
                    obj.getPlotId(),
                    obj.getGardener().getEmployee().getName(), null);
        }
        return new OutputMapObj(obj.getPlotId(), null, null);
    }


    public MapObj[][] makeGardenMatrix(Garden garden, List<Plot> plots){
        MapObj[][] matrix = new MapObj[garden.getWidth()][garden.getLength()];
        Gardener[][] gardeners = makeGardenersMatrix(garden);
        for(int i = 0; i<garden.getWidth(); i++) {
            for (int j = 0; j < garden.getLength(); j++) {
                matrix[i][j]=new MapObj();
            }
        }
        for (Plot p: plots) {
            for(int i = p.getFromX(); i<=p.getToX(); i++){
                for(int j = p.getFromY(); j<= p.getToY(); j++){
                    if(matrix[i][j]!= null){
                        System.out.println(p);
                        System.out.println("WRONG PLOT ARANGEMENT AT PLOT ID: "+p.getId());
                        //return null;
                    }
                    matrix[i][j].setPlotId(p.getId());
                    matrix[i][j].setGardener(gardeners[i][j]);
                    if(p.getPlant()!= null){
                        matrix[i][j].setPlant(p.getPlant());
                    }
                }
            }
        }
        return matrix;
    }
    private Gardener[][] makeGardenersMatrix(Garden garden){
        Gardener[][] matrix = new Gardener[garden.getWidth()][garden.getLength()];

        List<Gardener> gardenerList = gardenerRepo.getAll();
        for (Gardener g: gardenerList) {
            if(g.getTeam().getId() == garden.getTeam().getId()){
                Task t = g.getTask();
                if(t!= null) {
                    Plot p = t.getPlot();
                    if (p != null) {
                        for (int i = p.getFromX(); i <= p.getToX(); i++) {
                            for (int j = p.getFromY(); j <= p.getToY(); j++) {
                                matrix[i][j] = g;
                            }
                        }
                    }
                }
            }
        }
        return matrix;
    }

}
