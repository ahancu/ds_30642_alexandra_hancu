package server.services;

import model.Gardener;
import model.RefillRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.repos.RefillRequestRepo;

@Service
public class RefillRequestService {
    RefillRequestRepo refillRequestRepo;

    @Autowired
    public RefillRequestService(RefillRequestRepo refillRequestRepo){
        this.refillRequestRepo = refillRequestRepo;
    }

    public RefillRequest getByGardener(Gardener gardener){
        return refillRequestRepo.getByGardener(gardener);
    }

    public void newRequest(RefillRequest refillRequest){
        refillRequestRepo.save(refillRequest);
    }
    public RefillRequest updateRequest(RefillRequest refillRequest){
        return refillRequestRepo.update(refillRequest);
    }
}
