package server.services;

import client.IPrinter;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;

@Service
public class PdfPrinter implements IPrinter {

    public boolean print (String data, String path){
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(path+"\\report.pdf"));
            document.open();
            document.addTitle("Plant List");
            document.addSubject("Report");
            document.addKeywords("Java, PDF, iText");
            document.addAuthor("Hancu Alexandra");
            document.addCreator("Hancu Alexandra");
            document.addLanguage("English");
            Paragraph content = new Paragraph(data);
            document.add(content);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
