package server.services;

import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.repos.GardenRepo;
import server.repos.GardenerRepo;
import server.repos.PlantRepo;
import server.repos.PlotRepo;

import java.util.List;

@Service
public class GardenerService {
    private GardenRepo gardenRepo ;
    private GardenerRepo gardenerRepo ;
    private PlotRepo plotRepo;

    @Autowired
    public GardenerService(GardenRepo gardenRepo, GardenerRepo gardenerRepo, PlotRepo plotRepo){
        this.gardenerRepo = gardenerRepo;
        this.gardenRepo = gardenRepo;
        this.plotRepo = plotRepo;
    }

    public Garden getGarden(Gardener gardener) {
        return gardenRepo.getGarden4Team(gardener.getTeam());
    }

    public Gardener getGardener(Employee employee) {
        return gardenerRepo.getByEmployee(employee);
    }

    public List<Gardener> getAllGardeners() {
        return gardenerRepo.getAll();
    }

    public List<Plot> getPlots(){
        return plotRepo.getAll();
    }
}
