package server.services;

import model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.repos.EmployeeRepo;

@Service
public class LoginService {
    private EmployeeRepo employeeRepo;

    @Autowired
    public LoginService(EmployeeRepo employeeRepo) {
        this.employeeRepo = employeeRepo;
    }

    public boolean login(String username, String password) {
        if ("admin".equals(username) && "admin".equals(password)) {
            return true;
        }
        String passFromDB = employeeRepo.getPassword(username);
        if (passFromDB == null) {
            return false;
        }
        if (passFromDB.compareTo(password) == 0) {
            return true;
        }
        return false;
    }

    public Employee getEmployee(String username) {
        if (username == null || username.isEmpty()) {
            return null;
        }
        return employeeRepo.getByUsername(username);
    }

    public boolean isAdmin(String username) {
        return "admin".equals(username);
    }
}
