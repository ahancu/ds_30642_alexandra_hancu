package server.services;

import model.Garden;
import model.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.repos.GardenRepo;

@Service
public class GardenService {
    private GardenRepo gardenRepo;

    @Autowired
    public GardenService(GardenRepo gardenRepo){
        this.gardenRepo = gardenRepo;
    }

    public Garden getGarden(int gardenId) {
        return gardenRepo.getOne(gardenId);
    }

    public Garden getGarden(Team team) {
        return gardenRepo.getOne(team);
    }
}
