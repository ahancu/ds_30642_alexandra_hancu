package server.services;

import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.repos.*;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SupervisorService {

    private SupervisorRepo supervisorRepo;
    private PlantRepo plantRepo;
    private GardenerRepo gardenerRepo;
    private EmployeeRepo employeeRepo;
    private TeamRepo teamRepo;
    private PlotRepo plotRepo;
    private TaskRepo taskRepo;

    @Autowired
    public SupervisorService(SupervisorRepo supervisorRepo, PlantRepo plantRepo, GardenerRepo gardenerRepo,
                             EmployeeRepo employeeRepo, TeamRepo teamRepo, PlotRepo plotRepo, TaskRepo taskRepo) {
        this.supervisorRepo = supervisorRepo;
        this.plantRepo = plantRepo;
        this.gardenerRepo = gardenerRepo;
        this.employeeRepo = employeeRepo;
        this.teamRepo = teamRepo;
        this.plotRepo = plotRepo;
        this.taskRepo = taskRepo;
    }

    public Supervisor getSupervisor(Employee employee) {
        return supervisorRepo.getByEmployee(employee);
    }

    public List<Plant> getPlantList() {
        return plantRepo.getAll();
    }

    public Plant getPlant(int id) {
        return plantRepo.getOne(id);
    }

    public List<Gardener> getTeamMembers(Team team) {
        return gardenerRepo.getByTeam(team);
    }

    public Gardener getGardenerInfo(int id) {
        Gardener g = gardenerRepo.getOne(id);
        if(g == null)
            return null;
        return g;
    }

    public List<Team> getAllTeams(){
        return teamRepo.getAll();
    }

    public Plant newPlant(Plant p) {
        if(p == null) return null;
        return plantRepo.save(p);
    }

    public int deletePlant(Long id) {
        return plantRepo.delete(Integer.parseInt(""+id));
    }

    public int deleteGardener(Long id) {
        return gardenerRepo.delete(Integer.parseInt(""+id));
    }

    public boolean newGardener(Employee emp, Team t, Gardener gardener) {
        if(alreadyExists(emp)) return false; //username-ul si numele trebuie sa fie unice
        employeeRepo.save(emp);
        teamRepo.save(t);
        gardenerRepo.save(gardener);
        return true;
    }

    public Team getTeam(int id) {
        return teamRepo.getOne(id);
    }

    public boolean newGardener(Employee emp, Gardener gardener) {
        if(alreadyExists(emp)) return false; //username-ul si numele trebuie sa fie unice
        employeeRepo.save(emp);
        gardenerRepo.save(gardener);
        return  true;
    }

    public Gardener newGardener(OutputGardener gardener) {
        //if(employeeRepo.getByUsername(gardener.username) != null) return null;
        Employee newEmployee = new Employee();
        newEmployee.setName(gardener.name);
        newEmployee.setUsername(gardener.username);
        newEmployee.setPassword(gardener.password);
        Gardener newGardener = new Gardener();
        newGardener.setEmployee(employeeRepo.getOne(employeeRepo.save(newEmployee)));
        if(gardener.teamId != 0){
            newGardener.setTeam(teamRepo.getOne(gardener.teamId));
        } else {
            newGardener.setTeam(teamRepo.getOne(teamRepo.save(new Team())));
        }
        Gardener saved = gardenerRepo.getOne(gardenerRepo.save(newGardener));

        return saved;
    }

    private boolean alreadyExists(Employee employee){
        return employeeRepo.getByUsername(employee.getUsername()) != null
                || employeeRepo.getByName(employee.getName()) != null;
    }

    private Supervisor getSupervisorOfTeam(Team team){
        for (Supervisor s: supervisorRepo.getAll()){
            if(s.getTeam().getId() == team.getId()){
                return s;
            }
        }
        return null;
    }

    public Supervisor notify(Gardener gardener) {
        if(gardener == null)
            return null;
         Team team = gardener.getTeam();
         if(team == null)
             return null;
         Supervisor s = getSupervisorOfTeam(team);
         return s;
    }

    public Task newTask(Plot plot, int time, float reward, Gardener gardener) {
        if(plot!= null && gardener!= null) {
            Plot plotFromDB = plotRepo.getOne(plot.getId());
            Gardener gardenerFromDB = gardenerRepo.getOne(gardener.getId());
            Task task = new Task(plotFromDB, time, reward, gardenerFromDB);
            taskRepo.save(task);
            gardenerFromDB.setTask(task);
            gardenerRepo.update(gardenerFromDB.getId(), gardenerFromDB);
            return task;
        }

        return null;
    }

    public Supervisor updateSupervisor(Supervisor supervisor) {
        supervisorRepo.update(supervisor.getId(), supervisor);
        return supervisor;
    }

    public List<OutputGardener> remap(List<Gardener> allGardeners) {
        return allGardeners.stream()
                .map(gardener ->
                        new OutputGardener(
                                gardener.getId(),
                                gardener.getEmployee().getName(),
                                gardener.getEmployee().getUsername(),
                                gardener.getEmployee().getPassword(),
                                gardener.getTeam().getId()))
                .collect(Collectors.toList());
    }
}
