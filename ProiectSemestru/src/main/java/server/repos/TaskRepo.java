package server.repos;

import model.Task;
import model.Team;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TaskRepo {
    private SessionFactory sessionFactory = SqliteSessionFactory.buildSessionFactory();


    public void save(Task task){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(task);
        session.getTransaction().commit();
    }

    public void getAll(){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("from Task ");
        List<Task> tasks = query.list();
        for (Task t: tasks) {
            System.out.println(t);
        }
        session.getTransaction().commit();
    }

    public Task getOne(int id){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Task task = session.get(Task.class, id);
        session.getTransaction().commit();
        return task;
    }
}
