package server.repos;

import model.Plant;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PlantRepo {
    private SessionFactory sessionFactory = SqliteSessionFactory.buildSessionFactory();

    public Plant save(Plant plant){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(plant);
        session.getTransaction().commit();
        return plant;
    }
    public List<Plant> getAll(){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("from Plant");
        List<Plant> plants = query.list();
        for (Plant p: plants) {
            System.out.println(p);
        }
        session.getTransaction().commit();
        return plants;
    }

    public Plant getOne(int id){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Plant plant = session.get(Plant.class, id);
        session.getTransaction().commit();
        return plant;
    }

    public Plant update(int id, Plant p){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Plant plant = session.get(Plant.class, id);
        plant.setName(p.getName());
        plant.setDimension(p.getDimension());
        plant.setType(p.getType());
        plant.setWateringAmount(p.getWateringAmount());
        plant.setWateringInterval(p.getWateringInterval());
        plant.setNoSeeds(p.getNoSeeds());
        session.getTransaction().commit();
        return plant;
    }
    public int delete(int id){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Plant plant = session.load(Plant.class, id);
        session.delete(plant);
        session.getTransaction().commit();
        return id;
    }
}
