package server.repos;

import model.Employee;
import model.Supervisor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SupervisorRepo {
    private SessionFactory sessionFactory = SqliteSessionFactory.buildSessionFactory();


    public void save(Supervisor supervisor){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(supervisor);
        session.getTransaction().commit();
    }

    public List<Supervisor> getAll(){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("from Supervisor ");
        List<Supervisor> supervisors = query.list();
        for (Supervisor p: supervisors) {
            System.out.println(p);
        }
        session.getTransaction().commit();
        return supervisors;
    }

    public Supervisor getOne(int id){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Supervisor supervisor = session.get(Supervisor.class, id);
        session.getTransaction().commit();
        return supervisor;
    }

    public Supervisor update(int id, Supervisor s){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Supervisor supervisor = session.get(Supervisor.class, id);
        supervisor.setEmployee(s.getEmployee());
        supervisor.setTeam(s.getTeam());
        session.getTransaction().commit();
        return supervisor;
    }
    public void delete(int id){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Supervisor supervisor = session.load(Supervisor.class, id);
        session.delete(supervisor);
        session.getTransaction().commit();
    }

    public Supervisor getByEmployee(Employee employee) {
        List<Supervisor> supervisors = getAll();
        for (Supervisor s : supervisors) {
            if(s.getEmployee().equals(employee)){
                return s;
            }
        }
        return null;
    }
}
