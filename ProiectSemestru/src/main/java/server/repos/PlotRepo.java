package server.repos;

import model.Garden;
import model.Plot;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PlotRepo {
    private static SessionFactory sessionFactory = SqliteSessionFactory.buildSessionFactory();

    public void save(Plot plot){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(plot);
        session.getTransaction().commit();
    }

    public List<Plot> getAll(){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("from Plot");
        List<Plot> plots = query.list();
        for (Plot p: plots) {
            System.out.println(p);
        }
        session.getTransaction().commit();
        return plots;
    }

    public List<Plot> getAllFor(Garden garden){
        List<Plot> all = getAll();
        all.removeIf(plot -> plot.getGarden().getId() != garden.getId());
        return all;
    }

    public Plot getOne(int id){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Plot plot = session.get(Plot.class, id);
        session.getTransaction().commit();
        return plot;
    }

    public Plot update(int id, Plot p){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Plot plot = session.get(Plot.class, id);
        plot.setGarden(p.getGarden());
        plot.setPlant(p.getPlant());
        session.getTransaction().commit();
        return plot;
    }
    public void delete(int id){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Plot plot = session.load(Plot.class, id);
        session.delete(plot);
        session.getTransaction().commit();
    }
}
