package server.repos;

import model.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RefillRequestRepo {
    private SessionFactory sessionFactory = SqliteSessionFactory.buildSessionFactory();


    public void save(RefillRequest refillRequest){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(refillRequest);
        session.getTransaction().commit();
    }

    public List<RefillRequest> getAll(){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("from RefillRequest ");
        List<RefillRequest> requests = query.list();
        session.getTransaction().commit();
        return requests;
    }

    public RefillRequest getOne(int id){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        RefillRequest refillRequest = session.get(RefillRequest.class, id);
        session.getTransaction().commit();
        return refillRequest;
    }

    public RefillRequest update(RefillRequest request){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        RefillRequest refillRequest = getOne(request.getId());
        refillRequest.setRanking(request.getRanking());
        refillRequest.setGardener(request.getGardener());
        refillRequest.setPlants(request.getPlants());
        refillRequest.setSupervisor(request.getSupervisor());
        session.getTransaction().commit();
        return refillRequest;
    }

    public RefillRequest getByGardener(Gardener gardener){
        List<RefillRequest> all = getAll();
        for (RefillRequest request: all) {
            if(request.getGardener().equals(gardener)){
                return request;
            }
        }
        return null;
    }
}
