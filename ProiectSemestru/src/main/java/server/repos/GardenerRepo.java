package server.repos;

import model.Employee;
import model.Gardener;
import model.Team;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class GardenerRepo {
    private SessionFactory sessionFactory = SqliteSessionFactory.buildSessionFactory();


    public int save(Gardener gardener) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        int saved = (int) session.save(gardener);
        session.getTransaction().commit();
        return saved;
    }

    public List<Gardener> getAll() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("from Gardener ");
        List<Gardener> gardeners = query.list();
        for (Gardener g : gardeners) {
            System.out.println(g);
        }
        session.getTransaction().commit();
        return gardeners;
    }

    public Gardener getOne(int id) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Gardener gardener = session.get(Gardener.class, id);
        session.getTransaction().commit();
        return gardener;
    }

    public Employee getEmployee4Gardener(int id) {
        return getOne(id).getEmployee();
    }

    public Gardener update(int id, Gardener g) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Gardener gardener = session.get(Gardener.class, id);
        gardener.setEmployee(g.getEmployee());
        gardener.setMoney(g.getMoney());
        gardener.setTeam(g.getTeam());
        gardener.setTask(g.getTask());
        session.getTransaction().commit();
        return gardener;
    }

    public int delete(int id) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Gardener gardener = session.load(Gardener.class, id);
        session.delete(gardener);
        session.getTransaction().commit();
        return id;
    }

    public Gardener getByEmployee(Employee employee) {
        List<Gardener> gardeners = getAll();
        for (Gardener g : gardeners) {
            if (g.getEmployee().equals(employee)) {
                return g;
            }
        }
        return null;
    }

    public List<Gardener> getByTeam(Team team) {
        List<Gardener> gardeners = getAll();
        List<Gardener> result = new ArrayList<>();
        for (Gardener g : gardeners) {
            if (g.getTeam().getId() == team.getId()) {
                result.add(g);
            }
        }
        return result;
    }
}
