package server.repos;

import model.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EmployeeRepo {
    private static SessionFactory sessionFactory = SqliteSessionFactory.buildSessionFactory();


    public int save(Employee employee){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        int saved = (int) session.save(employee);
        session.getTransaction().commit();
        return saved;
    }

    public String getPassword(String username){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("from Employee");
        List<Employee> employees = query.list();
        for (Employee e: employees) {
            if(e.getUsername().compareTo(username) == 0){
                session.getTransaction().commit();
                return e.getPassword();
            }
        }
        session.getTransaction().commit();
        return null;
    }

    public void getAll(){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("from Employee");
        List<Employee> employees = query.list();
        for (Employee e: employees) {
            System.out.println(e);
        }
        session.getTransaction().commit();
    }

    public Employee getOne(int id){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Employee employee = session.get(Employee.class, id);
        session.getTransaction().commit();
        return employee;
    }

    public Employee update(int id, Employee e){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Employee employee = session.get(Employee.class, id);
        employee.setName(e.getName());
        employee.setPassword(e.getPassword());
        session.getTransaction().commit();
        return employee;
    }
    public void delete(int id){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Employee employee = session.load(Employee.class, id);
        session.delete(employee);
        session.getTransaction().commit();
    }

    public Employee getByUsername(String username) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("from Employee");
        List<Employee> employees = query.list();
        for (Employee e: employees) {
            if(username.compareTo(e.getUsername()) == 0){
                session.getTransaction().commit();
                return e;
            }
        }
        session.getTransaction().commit();
        return null;
    }

    public Employee getByName(String name) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("from Employee");
        List<Employee> employees = query.list();
        for (Employee e: employees) {
            if(e.getName().compareTo(name) == 0){
                session.getTransaction().commit();
                return e;
            }
        }
        session.getTransaction().commit();
        return null;
    }
}
