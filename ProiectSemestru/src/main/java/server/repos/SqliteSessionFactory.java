package server.repos;

import model.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;


public class SqliteSessionFactory {
    private static SessionFactory sessionFactory;

    public static org.hibernate.SessionFactory buildSessionFactory(){

        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Gardener.class);
        configuration.addAnnotatedClass(Plant.class);
        configuration.addAnnotatedClass(Employee.class);
        configuration.addAnnotatedClass(Garden.class);
        configuration.addAnnotatedClass(Plot.class);
        configuration.addAnnotatedClass(Supervisor.class);
        configuration.addAnnotatedClass(Team.class);
        configuration.addAnnotatedClass(Task.class);
        configuration.addAnnotatedClass(RefillRequest.class);
        configuration.configure("hibernate.cfg.xml");
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        return sessionFactory;

    }
}
