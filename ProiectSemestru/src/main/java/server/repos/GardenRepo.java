package server.repos;

import model.Garden;
import model.Team;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GardenRepo {
    private SessionFactory sessionFactory = SqliteSessionFactory.buildSessionFactory();


    public void save(Garden garden){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(garden);
        session.getTransaction().commit();
    }

    public List<Garden> getAll(){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("from Garden ");
        List<Garden> gardens = query.list();
        for (Garden g: gardens) {
            System.out.println(g);
        }
        session.getTransaction().commit();
        return gardens;
    }

    public Garden getGarden4Team(Team team){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(Garden.class);
        Garden garden = (Garden) criteria.add(Restrictions.eq("team", team)).uniqueResult();
        return garden;
    }

    public Garden getOne(int id){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Garden garden = session.get(Garden.class, id);
        session.getTransaction().commit();
        return garden;
    }
    public void delete(int id){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Garden garden = session.load(Garden.class, id);
        session.delete(garden);
        session.getTransaction().commit();
    }

    public Garden getOne(Team team) {
        List<Garden> all = getAll();
        for (Garden g: all){
            if(g.getTeam().getId() == team.getId()){
                return g;
            }
        }
        return null;
    }
}
