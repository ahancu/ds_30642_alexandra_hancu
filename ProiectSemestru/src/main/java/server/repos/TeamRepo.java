package server.repos;

import model.Team;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TeamRepo {
    private SessionFactory sessionFactory = SqliteSessionFactory.buildSessionFactory();


    public int save(Team team){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        int saved =(int) session.save(team);
        session.getTransaction().commit();
        return saved;
    }

    public List<Team> getAll(){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("from Team ");
        List<Team> teams = query.list();
        session.getTransaction().commit();

        return teams;
    }

    public Team getOne(int id){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Team team = session.get(Team.class, id);
        session.getTransaction().commit();
        return team;
    }

    public void delete(int id){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Team team = session.load(Team.class, id);
        session.delete(team);
        session.getTransaction().commit();
    }
}
