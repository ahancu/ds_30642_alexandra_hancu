package server.controllers;

import model.OutputGardener;
import model.OutputTeam;
import model.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import server.services.GardenerService;
import server.services.SupervisorService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class TeamController {
    private final SupervisorService supervisorService;

    @Autowired
    public TeamController(SupervisorService supervisorService) {
        this.supervisorService = supervisorService;
    }

    @RequestMapping(value = "/teams", method = GET)
    public @ResponseBody List<OutputTeam> getAllTeams(HttpServletRequest request) {
        return remap(supervisorService.getAllTeams());
    }

    private List<OutputTeam> remap(List<Team> allTeams) {
        List<OutputTeam> collect = allTeams.stream()
                .map(team -> new OutputTeam(
                        team.getId(),
                        supervisorService.remap(supervisorService.getTeamMembers(team))))
                .collect(Collectors.toList());
        return collect;
    }
}
