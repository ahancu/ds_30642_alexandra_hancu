package server.controllers;

import model.Gardener;
import model.OutputGardener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import server.services.GardenerService;
import server.services.SupervisorService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
public class GardenerController {
    private final GardenerService gardenerService;
    private final SupervisorService supervisorService;

    @Autowired
    public GardenerController(GardenerService gardenerService, SupervisorService supervisorService) {
        this.gardenerService = gardenerService;
        this.supervisorService = supervisorService;
    }

    @RequestMapping(value = "/gardeners", method = GET)
    public @ResponseBody
    List<OutputGardener> getAllGardeners(HttpServletRequest request) {
        return supervisorService.remap(gardenerService.getAllGardeners());
    }

    @RequestMapping(value = "/gardeners", method = POST)
    public @ResponseBody
    Gardener newGardener(@RequestBody OutputGardener gardener) {
        return supervisorService.newGardener(gardener);
    }

    @RequestMapping(value = "/gardeners/{id}", method = DELETE)
    public @ResponseBody
    Integer deleteGardener(@PathVariable Long id) {
        return supervisorService.deleteGardener(id);
    }
}
