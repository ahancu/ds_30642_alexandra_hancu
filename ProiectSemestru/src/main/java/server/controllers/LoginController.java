package server.controllers;

import model.LoginForm;
import model.StringResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import server.services.GardenerService;
import server.services.LoginService;
import server.services.SupervisorService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.springframework.web.bind.annotation.RequestMethod.*;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class LoginController {

    private final LoginService loginService;
    private final GardenerService gardenerService;
    private final SupervisorService supervisorService;
    private static boolean isLoggedIn = false;

    @Autowired
    public LoginController(LoginService loginService, GardenerService gardenerService, SupervisorService supervisorService) {
        this.loginService = loginService;
        this.gardenerService = gardenerService;
        this.supervisorService = supervisorService;
    }

    @RequestMapping(value = "/login", method = GET)
    public String index(HttpServletRequest request) {
        if (request.getSession().getAttribute("session") != null) {
            return "You are already logged in";
        }
        return "Login or register";
    }

    @RequestMapping(value = "/login", method = POST)
    public StringResponse postLogin(HttpServletRequest request, @RequestBody LoginForm login) {
        StringResponse response = new StringResponse();
        if (request.getSession().getAttribute("session") == null) {
            if (loginService.login(login.getUsername(), login.getPassword())) {
                request.getSession().setAttribute("session", "user");
                request.getSession().setAttribute("username", login.getUsername());
                isLoggedIn = true;
                response.setSuccess(true);
                response.setResponse("Welcome user: " + login.getUsername());
            } else {
                response.setSuccess(false);
                response.setResponse("Incorrect username/password");
                isLoggedIn = false;
            }
        } else {
            response.setSuccess(true);
            response.setResponse("You are already logged in");
        }
        return response;
    }

    @RequestMapping(value="/isAdmin/{username}", method = GET)
    public StringResponse isAdmin(@PathVariable(value="username") String id, @RequestParam String username){
        StringResponse response = new StringResponse();
        response.setSuccess(loginService.isAdmin(username));
        if(response.isSuccess()){
            response.setResponse(username + " is admin");
        }
        else{
            response.setResponse(username + " is regular user");
        }
        return response;
    }
    @RequestMapping(value="/logout", method = GET)
    public StringResponse logout(HttpServletRequest request){
        request.getSession().invalidate();
        StringResponse response = new StringResponse();
        response.setSuccess(true);
        response.setResponse("You have been logged out.");
        isLoggedIn = false;
        return response;
    }
}
