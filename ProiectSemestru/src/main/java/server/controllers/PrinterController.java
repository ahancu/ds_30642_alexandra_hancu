package server.controllers;

import model.OutputPrint;
import model.Plant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import server.services.GardenerService;
import server.services.PdfPrinter;
import server.services.PlantService;
import server.services.SupervisorService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class PrinterController {


    private final PdfPrinter pdfPrinter;
    private final SupervisorService supervisorService;

    private final GardenerService gardenerService;
    private final String path = "C:/Users/Alexandra/Downloads";

    @Autowired
    public PrinterController(PdfPrinter pdfPrinter, SupervisorService supervisorService, GardenerService gardenerService) {
        this.pdfPrinter = pdfPrinter;
        this.supervisorService = supervisorService;
        this.gardenerService = gardenerService;
    }


    @RequestMapping(value = "/report/gardener", method = GET)
    public @ResponseBody boolean reportOnGardeners(HttpServletRequest request) {
        return pdfPrinter.print(gardenerService.getAllGardeners().toString(), path);
    }

    @RequestMapping(value = "/report/plots", method = GET)
    public @ResponseBody boolean reportOnPlots(HttpServletRequest request) {
        return pdfPrinter.print(gardenerService.getPlots().toString(), path);
    }

    @RequestMapping(value = "/report/plants", method = GET)
    public @ResponseBody boolean reportOnPlants(HttpServletRequest request) {
        return pdfPrinter.print(supervisorService.getPlantList().toString(), path);
    }


}
