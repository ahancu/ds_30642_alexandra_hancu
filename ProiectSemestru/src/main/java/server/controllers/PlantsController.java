package server.controllers;

import model.OutputPlot;
import model.Plant;
import model.Plot;
import model.ToPlant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import server.services.PlantService;
import server.services.SupervisorService;

import javax.servlet.http.HttpServletRequest;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
public class PlantsController
{

    private final PlantService plantService;
    private final SupervisorService supervisorService;

    @Autowired
    public PlantsController(PlantService plantService, SupervisorService supervisorService) {
        this.plantService = plantService;
        this.supervisorService = supervisorService;
    }

    @RequestMapping(value = "/plants", method = GET)
    public @ResponseBody List<Plant> getAllPlants(HttpServletRequest request) {
        return plantService.getPlants();
    }

    @RequestMapping(value = "/plants", method = POST)
    public @ResponseBody Plant newPlant(@RequestBody Plant plant) {
        return supervisorService.newPlant(plant);
    }


    @RequestMapping(value = "/plants", method = PUT)
    public @ResponseBody
    OutputPlot plantSeeds(@RequestBody ToPlant toPlant) {
        return plantService.wrap(plantService.addPlant(toPlant.plantId, toPlant.plotId));
    }

    @RequestMapping(value = "/plants/{id}", method = DELETE)
    public @ResponseBody Integer deletePlant(@PathVariable Long id) {
        return supervisorService.deletePlant(id);
    }
}


