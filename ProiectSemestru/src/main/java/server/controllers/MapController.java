package server.controllers;

import model.MapObj;
import model.OutputMapObj;
import model.OutputTeam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import server.services.MapService;
import server.services.SupervisorService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class MapController {

    private final MapService mapService;

    @Autowired
    public MapController(MapService mapService) {
        this.mapService = mapService;
    }

    @RequestMapping(value = "/map", method = GET)
    public @ResponseBody OutputMapObj[][] getMap(HttpServletRequest request) {
        return mapService.makeMatrix();
    }
}
