package server;

import model.*;
import server.services.*;
import server.repos.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.Instant;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class Server extends Thread {
    private Socket socket;
    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;
    private GardenService gardenService = new GardenService(new GardenRepo());
    private PlantService plantService = new PlantService(new PlantRepo(), new PlotRepo());
    private MapService mapService = new MapService(new PlotRepo(), new GardenerRepo(), new GardenRepo());
    private SupervisorService supervisorService = new SupervisorService(new SupervisorRepo(), new PlantRepo(), new GardenerRepo(), new EmployeeRepo(), new TeamRepo(), new PlotRepo(), new TaskRepo());
    private GardenerService gardenerService = new GardenerService(new GardenRepo(), new GardenerRepo(), new PlotRepo());
    private LoginService loginService = new LoginService(new EmployeeRepo());
    private RefillRequestService refillRequestService = new RefillRequestService(new RefillRequestRepo());

    private static Supervisor who;
    private static String what;
    private static boolean notification = false;


    private static int clients = 0;

    private Server(Socket socket){
        this.socket = socket;
    }

    @Override
    public void run() {
        try {this.outputStream =new ObjectOutputStream (socket.getOutputStream());
             this.inputStream = new ObjectInputStream(socket.getInputStream());
            System.out.println("NEW CLIENT online now:" + (++clients));
            while (socket.isConnected()) {
                boolean clientHasData = socket.getInputStream().available() > 0;
                if (clientHasData) {
                    Bus request = (Bus) inputStream.readObject();
                    decodeRequest(request);
                }

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            clients--;
            System.out.println(Instant.now() + " client.Client disconnected?");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        // cleanup
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendNotification(){
        try {
            List<Object> data = new ArrayList<>();
            if(notification) {
                data.add(who);
                data.add(what);
            }
            Bus bus = new Bus(MessageType.notifcation, null, data);
            System.out.println("notification sent at "+ LocalTime.now());
            outputStream.writeObject(bus);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT SEND NOTIFICATION");
        }
    }

    private void closeAll(){
        try {
            outputStream.close();
            inputStream.close();
            socket.close();
        } catch (IOException e) {
            System.out.println("ERROR CLOSING SOCKET");
        }
    }

    private void decodeRequest(Bus bus){
        switch (bus.getMessageType()){
            case notifcation:
                notification = false;
                who = null;
                what = null;
                break;
            case login:
                callLogin(bus); break;
            case getEmployee:
                callGetEmployee(bus); break;
            case addPlant:
                callAddPlant(bus); break;
            case getPlant:
                callGetPlant(bus);break;
            case getTeamMembers:
                callGetTeamMembers(bus); break;
            case getGardenerInfo:
                callGetGardenerInfo(bus); break;
            case getPlantList:
                callGetPlants(bus); break;
            case getGarden:
                callGetGarden(bus); break;
            case getPlots:
                callGetPlots(bus); break;
            case makeGardenMatrix:
                callMakeMatrix(bus); break;
            case newPlant:
                callNewPlant(bus); break;
            case newGardener3:
                callNewGardener3(bus); break;
            case newGardener2:
                callNewGardener2(bus); break;
            case getTeam:
                callGetTeam(bus); break;
            case getGardenByTeam:
                callGetGardenByTeam(bus);break;
            case newTask:
                callNewTask(bus); break;
            case getGardenerByEmployee:
                callGetGardenerByEmployee(bus); break;
            case getSupervisor:
                callGetSupervisor(bus); break;
            case updatePlant:
                callUpdatePlant(bus); break;
            case updateSupervisor:
                callUpdateSupervisor(bus); break;
            case getRefillRequestByGardener:
                callGetRefillByGardener(bus); break;
            case newRequest:
                callNewRefillRequest(bus); break;
            case updateRequest:
                callUpdateRefillRequest(bus); break;
            default:
                System.out.println("unknown request"+bus);
        }
        if(bus.getMessageType()!=MessageType.notifcation)
            sendNotification();
    }

    private void callUpdateRefillRequest(Bus bus) {
        RefillRequest refillRequest = null;
        refillRequest = (RefillRequest) bus.getData().get(0);
        RefillRequest response = null;
        response = refillRequestService.updateRequest(refillRequest);
        try {
            outputStream.writeObject(response);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT SEND SUPERVISOR");
        }

    }

    private void callNewRefillRequest(Bus bus) {
        RefillRequest refillRequest = null;
        refillRequest = (RefillRequest) bus.getData().get(0);

        try {
            refillRequestService.newRequest(refillRequest);
            outputStream.writeBoolean(true);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT SEND SUPERVISOR");
        }
    }

    private void callGetRefillByGardener(Bus bus) {
        Gardener gardener = null;
        gardener = (Gardener) bus.getData().get(0);
        RefillRequest response = null;
        response = refillRequestService.getByGardener(gardener);
        try {
            outputStream.writeObject(response);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT SEND SUPERVISOR");
        }
    }

    private void callUpdateSupervisor(Bus bus) {
        Supervisor supervisor = null;
        supervisor = (Supervisor) bus.getData().get(0);
        Supervisor response = supervisorService.updateSupervisor(supervisor);
    }

    private void callUpdatePlant(Bus bus) {
        Plant plant = null;
        plant = (Plant) bus.getData().get(0);
        Plant response = plantService.updatePlant(plant);
        try {
            outputStream.writeObject(response);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE UPDATE PLANT RESPONSE");
        }
    }

    private void callGetSupervisor(Bus bus) {
        Employee employee = null;
        employee = (Employee) bus.getData().get(0);
        Supervisor supervisor = null;
        supervisor = supervisorService.getSupervisor(employee);
        try {
            outputStream.writeObject(supervisor);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT SEND SUPERVISOR");
        }
    }

    private void callGetGardenerByEmployee(Bus bus) {
        Gardener gardener = null;
        Employee employee = null;
        employee = (Employee) bus.getData().get(0);
        if(employee!= null)
            gardener = gardenerService.getGardener(employee);
        try {
            outputStream.writeObject(gardener);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT SEND GARDENER");
        }
    }

    private void callNewTask(Bus bus) {
        Plot plot= null;
        int time = 0;
        float reward = 0;
        Gardener gardener = null;
        Task task = null;
        plot = (Plot) bus.getData().get(0);
        time = (int) bus.getData().get(1);
        reward = (float) bus.getData().get(2);
        gardener = (Gardener) bus.getData().get(3);
        if(plot!=null && gardener!= null && time!=0 && reward!= 0){
            task = supervisorService.newTask(plot,time,reward,gardener);
        }
        try {
            outputStream.writeObject(task);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT SEND NEW TASK");
        }
    }

    private void callGetGardenByTeam(Bus bus) {
        Team team = null;
        Garden garden = null;
        team = (Team) bus.getData().get(0);
        if(team != null){
            garden = gardenService.getGarden(team);
        }
        try {
            outputStream.writeObject(garden);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT GET GARDEN BY TEAM");
        }

    }

    private synchronized void  callGetTeam(Bus bus) {
        Team team = null;
        int id = 0;
        id = (int) bus.getData().get(0);
        if(id != 0){
            team = supervisorService.getTeam(id);
        }
        try {
            outputStream.writeObject(team);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE TEAM");
        }
    }

    private synchronized void callNewGardener2(Bus bus) {
        Gardener gardener = null;
        Employee employee = null;
        boolean response = false;
        if(bus!= null && !bus.getData().isEmpty()){
            employee = (Employee) bus.getData().get(0);
            gardener = (Gardener) bus.getData().get(1);
        }
        if(gardener!= null && employee != null){
            response = supervisorService.newGardener(employee,gardener);
        }
        try {
            outputStream.writeBoolean(response);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE NEW GARDENER RESPONSE "+response);
        }
    }

    private synchronized void callNewGardener3(Bus bus) {
        Gardener gardener = null;
        Employee employee = null;
        Team team = null;
        boolean response = false;
        if(bus!= null && !bus.getData().isEmpty()){
            employee = (Employee) bus.getData().get(0);
            team = (Team) bus.getData().get(1);
            gardener = (Gardener) bus.getData().get(2);
        }
        if(gardener!= null && employee != null && team != null){
            response = supervisorService.newGardener(employee,team,gardener);
        }
        try {
            outputStream.flush();
            outputStream.writeBoolean(response);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE NEW GARDENER RESPONSE "+response);
        }
    }

    private synchronized void callNewPlant(Bus bus) {
        Plant plant= null;
        boolean response = false;
        if(bus!= null && !bus.getData().isEmpty()){
            plant = (Plant) bus.getData().get(0);
        }
        if(plant!= null){
            response = supervisorService.newPlant(plant) != null;
        }
        try {
            outputStream.writeBoolean(response);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE BOOLEAN "+ response);
        }
    }

    private synchronized void callMakeMatrix(Bus bus) {
        Garden garden = null;
        List<Plot> plots = null;
        Object[][] mapData = null;
        if(bus!= null && !bus.getData().isEmpty()){
            garden = (Garden) bus.getData().get(0);
            plots = (List<Plot>) bus.getData().get(1);
        }
        if(garden!= null && plots!= null) {
            mapData = mapService.makeGardenMatrix(garden, plots);
        }
        try {
            outputStream.writeObject(mapData);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRTIE MAPDATA");
        }
    }

    private synchronized void callGetPlots(Bus bus) {
        Garden garden = null;
        List<Plot> plots = null;
        if(bus!= null && !bus.getData().isEmpty()){
            garden = (Garden) bus.getData().get(0);
        }
        if(garden != null){
            plots = mapService.getPlots(garden);
        }
        try {
            outputStream.writeObject(plots);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE PLOT LIST");
        }
    }

    private synchronized void callGetGarden(Bus bus) {
        Garden garden = null;
        int id = 0;
        if(bus!= null && !bus.getData().isEmpty()){
            id = (int) bus.getData().get(0);
        }
        if(id != 0){
            garden = gardenService.getGarden(id);
        }
        try {
            outputStream.writeObject(garden);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE GARDEN"+garden);
        }
    }

    private synchronized void callGetPlants(Bus bus) {
        List<Plant> plants = plantService.getPlants();
        try {
            outputStream.writeObject(plants);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE PLANTS "+ plants);
        }
    }

    private synchronized void callGetGardenerInfo(Bus bus) {
        Gardener gardener = null;
        int id = 0;
        if(bus!= null && !bus.getData().isEmpty()){
            id = (int) bus.getData().get(0);
        }
        if(id != 0){
            gardener = supervisorService.getGardenerInfo(id);
        }
        try {
            outputStream.writeObject(gardener);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE GARDENER "+gardener);
        }
    }

    private synchronized void callGetTeamMembers(Bus bus) {
        Team team = null;
        if(bus!= null && !bus.getData().isEmpty()){
            team =  (Team) bus.getData().get(0);
        }
        List<Gardener> gardeners = null;
        if(team != null){
            gardeners = supervisorService.getTeamMembers(team);
        }
        try {
            outputStream.writeObject(gardeners);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT WRITE GARDENERS LIST" + gardeners);
        }
    }

    private synchronized void callGetPlant(Bus bus) {
        int id = 0;
        Plant plant = null;
        if(bus!= null && !bus.getData().isEmpty()){
            id = (int) bus.getData().get(0);
        }
        if(id != 0){
            plant = supervisorService.getPlant(id);
        }
        try {
            outputStream.writeObject(plant);
        } catch (IOException e) {
            System.out.println("ERROR IN METHOD CALL GET PLANT-IOE");
        }
    }

    private synchronized void callAddPlant(Bus bus) {
        Plant plant = null;
        Plot plot = null;
        Plot response = null;
        if(bus!= null && !bus.getData().isEmpty()){
            plot = (Plot) bus.getData().get(0);
            plant = (Plant) bus.getData().get(1);
        }
        if(plot!= null && plant!= null){
            response = plantService.addPlant(plot, plant);
        }
        try {
            outputStream.writeBoolean(response!= null);
        } catch (IOException e) {
            System.out.println("ERROR! CANNOT READ PLOT/PLANT OBJ!-IOE");
        }
    }

    private synchronized void callGetEmployee(Bus bus) {
        String username = "";
        if(bus!= null && !bus.getData().isEmpty()) {
            username = bus.getData().get(0).toString();
        }
        try {
            outputStream.writeObject(loginService.getEmployee(username));
        } catch (IOException e) {
            System.out.println("ERROR! METHOD: callGetEmployee-IOE");
        }
    }

    private synchronized void callLogin(Bus bus) {
        String username = "";
        String password = "";
        boolean loggedIn = false;
        if(bus!= null && !bus.getData().isEmpty()) {
            username = bus.getData().get(0).toString();
            password = bus.getData().get(1).toString();
        }
        if(!username.isEmpty() && !password.isEmpty()){
            loggedIn = loginService.login(username,password);
        }

        if(loggedIn){
            Supervisor s = supervisorService.notify(gardenerService.getGardener(loginService.getEmployee(username)));
            if(s != null) {
                notification = true;
                who = s;
                what = username;
            }
        }
        System.out.println("logged in? "+loggedIn);

        try {
            outputStream.writeBoolean(loggedIn);
            outputStream.flush();
            System.out.println("sent"+ LocalTime.now());
        } catch (IOException e) {
            System.out.println("ERROR! METHOD: callLogin - IOE");
        }
    }

    public static void main(String[] args) {

        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(1342);
            while(true) {
                new Server(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            System.out.println("ERROR ACCEPTING SOCKET");
        }

    }
}
