public class Autor {
    private String nume;

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public Autor(String nume) {
        this.nume = nume;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Autor autor = (Autor) o;

        return nume != null ? nume.equals(autor.nume) : autor.nume == null;
    }

    @Override
    public int hashCode() {
        return nume != null ? nume.hashCode() : 0;
    }
}
